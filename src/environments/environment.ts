// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: "AIzaSyBWHNx5s-Ug-IQtZOWTW71KLTX-zgeWJGk",
    authDomain: "dreamzest-4baf2.firebaseapp.com",
    projectId: "dreamzest-4baf2",
    storageBucket: "dreamzest-4baf2.appspot.com",
    messagingSenderId: "850169807043",
    appId: "1:850169807043:web:e1df4e97293bc00655e276",
    measurementId: "G-EPEXXDN3K0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
