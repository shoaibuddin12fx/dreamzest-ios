class globalData {
  InteriorColors = [];
  ExteriorColors = [];
  allStores = [];
  selectedStore = 13;
  Employees = [];
  Vendors = [];
  catagories = [];
  costTypes = [];
  payTypes = [];
  allStates = [];

  InvDetailCostAddCostScreenObjects = null;
  InvDetailCostAddCostScreenVehicleSource = null;
  invOption = null;
  userData = {};
  image = 'https://www.automaticlot.com';
  crComponentIds = [];

  BASE_URL = 'https://dreamzest.thesupportonline.net/api';
  API_KEYWORD = '/keyword';
  API_KEYWORD_SEARCH = '/keywordSearch';
  API_INTERPRETATION = '/interpretation/';
  API_SUBSCRIBE = 'subscribe';
  API_SUBSCRIBE_APPLE = 'subscribe/apple';
  API_CANCEL_SUBSCRIPTION = 'cancel-subscription';
  RC_WEB_PAYMENT = 1523;
  PLAN_ID = 'price_1Jc7UVDCbYLFSlBy17WIewSj';
}

export const global = new globalData();
