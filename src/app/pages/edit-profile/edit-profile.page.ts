import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  loading = false;
  userData = {
    displayName: '',
    name: '',
    phoneNumber: '',
    email: '',
    password: '',
  };

  constructor(
    public auth: AuthenticationService,
    public utility: UtilityService,
    public afStore: AngularFirestore,
    public nav: NavService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.getUser();
  }

  async updateUserData() {
    const user = JSON.parse(localStorage.getItem('user'));
    this.utility.showLoader();
    const res = await this.auth.SetUserData(user, this.userData);
    this.utility.hideLoader();
    this.utility.presentSuccessToast('Profile successfully saved');
    this.nav.pop();
    console.log(res);
  }

  async getUser() {
    this.loading = true;
    let user = await this.auth.getUser();
    console.log('Got User', user);

    this.afStore
      .doc(`users/` + user['uid'])
      .valueChanges()
      .subscribe(
        (snapshot) => {
          this.loading = false;
          console.log('USER_CHANGES', snapshot);
          this.userData.name = snapshot['name'];
          this.userData.displayName = snapshot['displayName'];
          this.userData.phoneNumber = snapshot['phoneNumber'];
          this.userData.email = snapshot['email'];
        },
        (error) => {
          this.loading = false;
          console.log('getUser', error.toString());
          alert(error.toString());
          // res(null);
        }
      );
  }
}
