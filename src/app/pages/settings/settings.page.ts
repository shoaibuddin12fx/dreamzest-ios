import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ModalService } from 'src/app/services/basic/modal.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsComponent } from './terms/terms.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  constructor(
    public auth: AuthenticationService,
    public nav: NavService,
    public modals: ModalService
  ) {}

  ngOnInit() {}

  logout() {
    return new Promise(async (resolve) => {
      const res = await this.auth.SignOut();
      console.log('logout', res);
      resolve(res);
    });
  }

  edeitProfile() {
    this.nav.push('edit-profile');
  }

  renewSubscription() {
    this.nav.push('iap-subscription');
  }

  openTerm() {
    this.modals.present(TermsComponent);
  }

  openPrivacy() {
    this.modals.present(PrivacyPolicyComponent);
  }
}
