import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.scss'],
})
export class DesignComponent implements OnInit {
  @Output('action') action: EventEmitter<any> = new EventEmitter<any>();
  constructor() {}

  ngOnInit() {}

  start() {
    this.action.emit({ key: 'startnow' });
  }
}
