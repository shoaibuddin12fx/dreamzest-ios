import { Component, OnInit } from '@angular/core';
import { FirebaseAuthentication } from '@awesome-cordova-plugins/firebase-authentication/ngx';
import { ViewWillEnter } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { UtilityService } from 'src/app/services/utility.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ModalService } from 'src/app/services/basic/modal.service';
import { SubscribeTutorialComponent } from 'src/app/components/subscribe-tutorial/subscribe-tutorial.component';
import { IapSubscriptionPage } from '../iap-subscription/iap-subscription.page';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements ViewWillEnter {
  loading = true;
  user = {};
  step = false;
  subscripttion_id;

  constructor(
    public auth: AuthenticationService,
    public nav: NavService,
    public utility: UtilityService,
    public afStore: AngularFirestore,
    private firebaseAuthentication: FirebaseAuthentication,
    private modal: ModalService //
  ) {}

  startNow($event) {
    this.modal.present(IapSubscriptionPage);
  }

  ionViewWillEnter() {
    this.getUser();
  }

  ngOnInit() {}

  async logout() {
    this.loading = true;
    const res = await this.auth.SignOut();
    console.log('login', res);
    this.loading = false;
    this.nav.push('login');
  }

  async books() {
    if (this.subscripttion_id)
      this.nav.push('books', { subscripttion_id: this.subscripttion_id });
    else await this.modal.present(SubscribeTutorialComponent);
    // this.nav.push('iap-subscription');
  }

  async renew() {
    this.nav.push('renew-subscription');
  }

  // async getUser() {
  //   this.utility.showLoader();
  //   let user = await this.auth.getUserData();
  //   this.user = user;
  //   localStorage.setItem('user', JSON.stringify(user));
  //   console.log(user);
  //   this.utility.hideLoader();
  //   if (user['subscription_id']) {
  //     this.step = true;
  //   }
  // }

  async getUser() {
    this.loading = true;
    let user = await this.auth.getUser();
    this.loading = false;
    console.log('Got User', user);

    this.loading = true;
    this.afStore
      .doc(`users/` + user['uid'])
      .valueChanges()
      .subscribe(
        (snapshot) => {
          console.log('USER_CHANGES', snapshot);

          this.loading = false;
          if (snapshot['subscription_id']) {
            this.subscripttion_id = snapshot['subscription_id'];
            this.step = true;
          } else this.step = false;

          // res(snapshot);
        },
        (error) => {
          this.loading = false;
          console.log('getUser', error.toString());
          alert(error.toString());
          // res(null);
        }
      );
  }
}
