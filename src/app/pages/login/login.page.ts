import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { UtilityService } from 'src/app/services/utility.service';
import { FirebaseAuthentication } from '@awesome-cordova-plugins/firebase-authentication/ngx';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loading = false;

  constructor(
    public auth: AuthenticationService,
    public nav: NavService,
    private utilitiy: UtilityService,
    private firebaseAuthentication: AngularFireAuth
  ) {}
  email = ''; //'josh@josh.com';
  password = ''; // 'abc123';

  ngOnInit() {}

  async login() {
    var user;
    this.loading = true;
    if (Capacitor.getPlatform() === 'web')
      user = await this.auth.SignInWeb(this.email, this.password);
    // else user = await this.auth.SignIniOS(this.email, this.password);
    else {
      console.log('here it is also running');
      user = await this.auth.SignInWeb(this.email, this.password);
    }
    this.loading = false;
    this.handleUser(user);
  }

  handleUser(user) {
    if (user) {
      localStorage.setItem('user', JSON.stringify(user));
      this.nav.push('dashboard-tabs');
    } else this.utilitiy.presentToast('No user found');
  }

  async login1() {
    // this.utilitiy.showLoader();
    // try {
    //   this.firebaseAuthentication
    //     .signInWithEmailAndPassword(this.email, this.password)
    //     .then(async (user) => {
    //       // let _user = await this.firebaseAuthentication.user;
    //       console.log('user', _user);
    //       if (_user) {
    //         localStorage.setItem('user', JSON.stringify(user));
    //         this.nav.push('dashboard-tabs');
    //       } else this.utilitiy.presentToast('No user found');
    //       //alert('Success');
    //     })
    //     .catch((reason) => {
    //       alert(reason);
    //     });
    // } catch (e) {
    //   console.log(e);
    //   this.utilitiy.presentToast(e.toString());
    // }
    // // this.utilitiy.hideLoader();
  }

  forgatePass() {
    return new Promise(async (resolve) => {
      const res = await this.auth.PasswordRecover(this.email);
      console.log('forgatePass', res);
      resolve(res);
    });
  }

  signUp() {
    this.nav.push('signup');
  }
}
