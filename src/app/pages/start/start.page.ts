import { Component, Injector, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage implements OnInit {
  loading = true;
  constructor(
    public nav: NavService,
    public auth: AuthenticationService,
    public utility: UtilityService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    // this.utility.showLoader();
    this.loading = true;
    this.auth.isUserLoggedIn().then((resolve) => {
      this.loading = false;
      if (resolve) {
        this.nav.push('dashboard-tabs');
        // this.utility.hideLoader();
      } else {
        // this.utility.hideLoader();
      }
    });
  }

  signUp() {
    this.nav.push('signup');
  }

  login() {
    this.nav.push('login');
  }
}
