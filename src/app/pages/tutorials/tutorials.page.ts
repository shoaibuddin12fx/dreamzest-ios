import { Component, Injector, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { UtilityService } from 'src/app/services/utility.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorials.page.html',
  styleUrls: ['./tutorials.page.scss'],
})
export class TutorialsPage implements OnInit {
  step = 1;
  constructor(
    public nav: NavService,
    public auth: AuthenticationService,
    public utility: UtilityService
  ) {}

  ngOnInit() {}

  next() {
    this.step++;
  }

  start() {
    this.auth.isUserLoggedIn().then((resolve) => {
      // this.loading = false;
      if (resolve) {
        this.nav.push('dashboard-tabs');
        // this.utility.hideLoader();
      } else {
        this.nav.push('start');
        // this.utility.hideLoader();
      }
    });
  }
}
