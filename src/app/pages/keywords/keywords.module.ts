import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KeywordsPageRoutingModule } from './keywords-routing.module';

import { KeywordsPage } from './keywords.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KeywordsPageRoutingModule
  ],
  declarations: [KeywordsPage]
})
export class KeywordsPageModule {}
