import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KeywordsPage } from './keywords.page';

const routes: Routes = [
  {
    path: '',
    component: KeywordsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KeywordsPageRoutingModule {}
