import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss'],
})
export class SubscriptionComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}

  subscribe() {}
}
