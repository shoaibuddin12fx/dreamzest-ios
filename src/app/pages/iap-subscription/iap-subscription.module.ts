import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IapSubscriptionPageRoutingModule } from './iap-subscription-routing.module';

import { IapSubscriptionPage } from './iap-subscription.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { SubscriptionComponent } from './subscription/subscription.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IapSubscriptionPageRoutingModule,
    HeaderModule,
  ],
  declarations: [IapSubscriptionPage, SubscriptionComponent],
})
export class IapSubscriptionPageModule {}
