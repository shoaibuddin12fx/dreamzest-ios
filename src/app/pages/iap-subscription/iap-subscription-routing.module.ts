import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IapSubscriptionPage } from './iap-subscription.page';

const routes: Routes = [
  {
    path: '',
    component: IapSubscriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IapSubscriptionPageRoutingModule {}
