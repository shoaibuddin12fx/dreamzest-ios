import { Component, Injector, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ModalService } from 'src/app/services/basic/modal.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { InAppPurchaseService } from 'src/app/services/iap.service';
import { NetworkService } from 'src/app/services/network.service';
import { UtilityService } from 'src/app/services/utility.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-iap-subscription',
  templateUrl: './iap-subscription.page.html',
  styleUrls: ['./iap-subscription.page.scss'],
})
export class IapSubscriptionPage implements OnInit {
  loading = false;
  products: any;
  productPurchased: any;
  isLoading = true;
  slides = [];
  product;
  // products = [
  //   {
  //     title: 'Product Title',
  //     description: 'Product description',
  //     price: 10,
  //   },
  // ];
  constructor(
    public iapService: InAppPurchaseService,
    public platform: Platform,
    public netwrork: NetworkService,
    public utility: UtilityService,
    public auth: AuthenticationService,
    public nav: NavService,
    public modal: ModalService
  ) {}

  get store() {
    return this.iapService.store;
  }

  back() {
    this.modal.dismiss();
  }

  ngOnInit() {
    this.initSlides();
    this.platform.ready().then((a) => {
      this.initialize();
    });
  }

  initSlides() {
    this.slides = [
      {
        title: 'Dreams Interpretation',
        subtitle:
          'Find your dreams interpretaion online at the most affordable price',
        icon: 'cloudy-night',
      },
      {
        title: 'Search Keywords',
        subtitle: 'Search from thousands of keywords according to your dream',
        icon: 'search',
      },
      {
        title: 'Religious Solution',
        subtitle: 'Find Biblical as well as Islamic solutions to your dreams',
        icon: 'checkmark-done',
      },
    ];
  }

  initialize() {
    this.iapService.initialize();
    this.iapService.productListener.subscribe((e) => {
      this.isLoading = false;

      this.products = e.filter(
        (product) => product.title && product.title !== ''
      );
      this.product = this.products[0];
    });

    this.iapService.productPurchasedListener.subscribe(async (p) => {
      if (!p || !p.transaction?.id) return;
      let user = JSON.parse(localStorage.getItem('user'));
      console.log(user);

      let data = {
        subscriptionID: p.transaction.id,
        plan_id: p.id,
        subs_user_id: user.uid,
        email: user.email,
        payer_id: user.uid, // temporary
        amount: p.price.match(/\d+/)[0],
        plan_interval: 365, // temporary
      };
      console.log('iap_data', data);
      let res = await this.netwrork.subscribeApple(data);
      if (res && res.success === true) {
        this.utility.presentSuccessToast(res.message ?? 'Success');
        var _user = await this.auth.getUser();
        const res1 = await this.auth.SetUserData(_user, {
          subscription_id: res.data.subscription_id,
        });
        this.nav.push('dashboard-tabs');
      }
      console.log('subscribeApple', res);
    });
  }

  refresh() {
    this.store.refresh();
  }

  subscribe() {
    let item = this.product;
    this.isLoading = true;
    this.iapService.purchase(item);
    this.isLoading = false;
    console.log(item.id);
  }

  openLink(type) {
    if (type === 'privacy') {
      window.open('https://dreamzest.com/privacy-policy');
    } else if (type === 'terms') {
      window.open('https://dreamzest.com/terms-of-use');
    }
  }
}
