import { Injector } from '@angular/core';
import { Location } from '@angular/common';
import { Platform, MenuController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalService } from 'src/app/services/basic/modal.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { SqliteService } from 'src/app/services/sqlite.service';
import { UsersService } from 'src/app/services/users.service';
import { UtilityService } from 'src/app/services/utility.service';
import { EventsService } from 'src/app/services/basic/events.service';
import { NetworkService } from 'src/app/services/network.service';
import { StoresService } from 'src/app/services/stores.service';
import { global } from 'src/app/config/constants';

export abstract class BasePage {

    public modals: ModalService;
    public nav: NavService;    
    public sqlite: SqliteService;
    public utility: UtilityService;
    public events: EventsService;
    public network: NetworkService;  
    public stores: StoresService;  

    allStores = [];  
    selectedStore;
    
    constructor(injector: Injector) {
        this.nav = injector.get(NavService);
        this.modals = injector.get(ModalService);
        this.sqlite = injector.get(SqliteService);
        this.utility = injector.get(UtilityService);
        this.events = injector.get(EventsService);
        this.network = injector.get(NetworkService);        
        this.stores = injector.get(StoresService);


        this.allStores = global.allStores; // await this.stores.getStoresFromSqlite() as [];
        this.selectedStore = global.selectedStore;
    }


}
