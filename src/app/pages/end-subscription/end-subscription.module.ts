import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EndSubscriptionPageRoutingModule } from './end-subscription-routing.module';

import { EndSubscriptionPage } from './end-subscription.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EndSubscriptionPageRoutingModule
  ],
  declarations: [EndSubscriptionPage]
})
export class EndSubscriptionPageModule {}
