import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EndSubscriptionPage } from './end-subscription.page';

const routes: Routes = [
  {
    path: '',
    component: EndSubscriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EndSubscriptionPageRoutingModule {}
