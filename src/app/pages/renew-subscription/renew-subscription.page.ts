import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Platform } from '@ionic/angular';
import { global } from 'src/app/config/constants';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { NetworkService } from 'src/app/services/network.service';
import { UtilityService } from 'src/app/services/utility.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-renew-subscription',
  templateUrl: './renew-subscription.page.html',
  styleUrls: ['./renew-subscription.page.scss'],
})
export class RenewSubscriptionPage implements OnInit {
  loading = false;
  // cardNumber = '';
  // month = '';
  // year = '';
  // cvc = '';
  user;
  isiOS;
  aForm: FormGroup;

  constructor(
    public network: NetworkService,
    public auth: AuthenticationService,
    public nav: NavService,
    public utility: UtilityService,
    public platform: Platform
  ) {}

  async ngOnInit() {
    this.isiOS = this.platform.is('ios');
    this.initForm();
  }

  initForm() {
    this.aForm = new FormGroup({
      cardNumber: new FormControl('', [
        Validators.required,
        Validators.minLength(15),
        Validators.maxLength(16),
        Validators.pattern(/^[+]*[(]{0,1}[1-9]{1,4}[)]{0,1}[-\s\./0-9]*$/),
      ]),
      month: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(2),
      ]),
      year: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(4),
      ]),
      cvv: new FormControl('', [
        Validators.required,
        Validators.maxLength(3),
        Validators.minLength(3),
      ]),
    });
  }

  async ionViewWillEnter() {
    this.loading = true;
    this.user = await this.auth.getUser();
    console.log('user', this.user.uid);
    this.loading = false;
  }

  applePay() {
    this.nav.push('iap-subscription');
  }

  async validate() {
    // console.log(this.cardNumber, this.month, this.cvc);

    // if (!this.cardNumber || !this.month || !this.cvc)
    //   alert('Please fill all fields');

    if (!this.aForm.valid) alert('Empty or invalid fields');
    else this.postData();
  }

  async postData() {
    this.loading = true;
    // console.log(this.month.split('/')[0].trim());
    // return;

    let form = this.aForm;
    let expiry = this.aForm.get('expiry').value;
    let res = await this.network.subscribe({
      card_no: form.get('cardNumber').value,
      exp_mon: form.get('month').value, //expiry.split('/')[0].trim(),
      exp_year: form.get('year').value, //expiry.split('/')[1].trim(),
      cvv: form.get('cvv').value,
      plan: global.PLAN_ID,
      subs_user_id: this.user.uid,
      email: this.user.email,
    });
    this.loading = false;
    console.log('subId', res);
    if (res && res.success === true) {
      this.utility.presentSuccessToast(res.message ?? 'Success');
      const res1 = await this.auth.SetUserData(this.user, {
        subscription_id: res.data.subscription_id,
      });

      this.nav.pop();
    }

    // else
    // this.utility.presentFailureToast(res?.message ?? 'Something went wrong');
  }

  onExpiryChanged($event) {
    let val = $event.target.value;
    console.log(val);
    if (val && val.length >= 3) {
      val = val.replace('/', '');
      val = this.insert(val, 2, '/');
      this.aForm.get('expiry').setValue(val);
    } else {
      val = val.replace('/', '');
      this.aForm.get('expiry').setValue(val);
    }
  }

  insert(str, index, value) {
    return str.substr(0, index) + value + str.substr(index);
  }
}
