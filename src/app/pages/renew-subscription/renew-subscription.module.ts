import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RenewSubscriptionPageRoutingModule } from './renew-subscription-routing.module';

import { RenewSubscriptionPage } from './renew-subscription.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { CreditCardDirectivesModule } from 'angular-cc-library';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RenewSubscriptionPageRoutingModule,
    HeaderModule,
    CreditCardDirectivesModule,
    ReactiveFormsModule,
  ],
  declarations: [RenewSubscriptionPage],
})
export class RenewSubscriptionPageModule {}
