import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/basic/modal.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { NetworkService } from 'src/app/services/network.service';
import { UtilityService } from 'src/app/services/utility.service';
import { ItemComponent } from './item/item.component';

@Component({
  selector: 'app-books',
  templateUrl: './books.page.html',
  styleUrls: ['./books.page.scss'],
})
export class BooksPage implements OnInit {
  subscription_id;
  last_page = 0;
  currentPage = 1;
  constructor(
    public modals: ModalService,
    public network: NetworkService,
    public utility: UtilityService,
    public nav: NavService
  ) {}

  allItems = [];
  items = [];
  search = '';

  ngOnInit() {
    this.subscription_id = this.nav.getQueryParams()?.subscripttion_id;
    this.getKeywords();
  }

  async getKeywords() {
    this.utility.showLoader();
    const res = await this.network.getKeywords(
      this.subscription_id,
      this.currentPage
    );
    this.utility.hideLoader();
    if (res && res.success && res.data) {
      console.log('Keywords', res);
      this.allItems.push(...res.data.data);
      this.items = this.allItems;
    } else this.utility.presentToast(res?.message ?? 'Something went wrong');
  }

  async openItem(id) {
    this.utility.showLoader();
    const res = await this.network.getinterpretation(this.subscription_id, id);
    this.utility.hideLoader();
    console.log(res);
    if (res.success) this.modals.present(ItemComponent, { data: res.data });
    else {
      this.utility.presentFailureToast(res?.message ?? 'Something went wrong');
    }
  }

  async searchKeywords() {
    const res = await this.network.searchKeywords(
      this.subscription_id,
      1,
      this.search
    );
    console.log(res);
    if (res.data != null) {
      console.log('TRUE');
      this.items = res.data.data;
    } else if (res.data == null && this.search == '') {
      console.log('else if');
      this.items = this.allItems;
    } else {
      console.log('FALSE');
      this.items = [];
      this.utility.presentFailureToast(res?.message ?? 'Something went wrong');
    }
  }

  loadMore() {
    this.currentPage++;
    this.getKeywords();
  }
}
