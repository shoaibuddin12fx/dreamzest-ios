import { Component, Input, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/basic/modal.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {


  @Input() data;

  constructor(public modals: ModalService) { }

  ngOnInit() {
    console.log(this.data)
  }

}
