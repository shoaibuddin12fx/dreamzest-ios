import { Component, OnInit } from '@angular/core';
import { FirebaseAuthentication } from '@awesome-cordova-plugins/firebase-authentication/ngx';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  loading = false;

  constructor(
    // private firebaseAuthentication: FirebaseAuthentication,
    public nav: NavService,
    public auth: AuthenticationService,
    private utilitiy: UtilityService
  ) {}

  email = '';
  password = '';
  userName = '';
  name = '';
  userPhoneNo = '';

  userData = {
    uid: '',
    email: '',
    displayName: '',
    name: '',
    phoneNumber: '',
  };

  ngOnInit() {}

  Signup() {
    this.loading = true;

    this.auth
      .RegisterUser(this.email, this.password)
      .then(async (success) => {
        console.log('Signup', success);
        this.email = '';
        this.password = '';
        let user = await this.auth.getUser();
        this.loading = false;
        if (user) {
          //alert('Success');
          console.log('user', user);
          this.nav.push('dashboard-tabs');
          this.setUserData(user);
        } else this.utilitiy.presentToast('Error occured');
      })
      .catch((reason) => {
        this.loading = false;
        this.utilitiy.presentFailureToast(reason.toString());
      });
  }

  setUserData(user) {
    return new Promise(async (resolve) => {
      this.userData.uid = user.uid;
      this.userData.email = user.email;
      this.userData.displayName = this.userName;
      this.userData.phoneNumber = this.userPhoneNo;
      this.userData.name = this.name;
      this.loading = true;
      const res = await this.auth.SetUserData(user, this.userData);
      this.loading = false;
      console.log('setUserData', res);
      this.email = '';
      resolve(res);
    });
  }

  login() {
    this.nav.push('login');
  }
}
