import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/basic/modal.service';
import { NavService } from 'src/app/services/basic/nav.service';

@Component({
  selector: 'app-subscribe-tutorial',
  templateUrl: './subscribe-tutorial.component.html',
  styleUrls: ['./subscribe-tutorial.component.scss'],
})
export class SubscribeTutorialComponent implements OnInit {
  step = 1;
  constructor(public nav: NavService, public modal: ModalService) {}

  ngOnInit() {}

  next() {
    if (this.step < 3) this.step++;
    else {
      this.nav.push('iap-subscription');
      this.modal.dismiss({ data: 'A' });
    }
  }

  close() {
    this.modal.dismiss({ data: 'A' });
  }
}
