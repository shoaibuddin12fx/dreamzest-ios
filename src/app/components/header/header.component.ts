import { Component, Input, OnInit } from '@angular/core';
import { NavService } from 'src/app/services/basic/nav.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() title;
  @Input() backVisible = true;
  @Input() loading = false;
  @Input() transparentHeader = false;

  constructor(public nav: NavService) {}

  ngOnInit() {}

  back() {
    this.nav.pop();
  }
}
