// import { Injectable } from '@angular/core';
// import { UtilityService } from './utility.service';
// import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
// import { NetworkService } from './network.service';
// import { ActionSheetController } from '@ionic/angular';

// @Injectable({
//   providedIn: 'root',
// })
// export class ScanService {
//   crLoaded: any;
//   data;
//   vin;
//   constructor(
//     private utility: UtilityService,
//     private network: NetworkService,
//     public actionSheetController: ActionSheetController
//   ) {}

//   scanCode() {
//     return new Promise(async (resolve) => {
//       let data = await this.hitScan();
//       resolve(data);
//     });
//   }

//   hitScan() {
//     return new Promise(async (resolve) => {
//       const flag = await didUserGrantPermission();

//       if (flag) {
//         // BarcodeScanner.hideBackground(); // make background of WebView transparent
//         // document.body.style.opacity = '0';

//         const result = await BarcodeScanner.startScan(); // start scanning and wait for a result
//         this.vin = result.content;
//         // setTimeout(() => {
//         BarcodeScanner.stopScan();

//         //   // document.body.style.opacity = '1';
//         // }, 2000)

//         // if the result has content

//         console.log('log result', result);

//         if (result.hasContent) {
//           console.log(result.content); // log the raw scanned content
//           // const res = await this.checkVin(result.content)
//           resolve(result.content);
//         } else {
//           resolve(null);
//         }
//       } else {
//         resolve(null);
//       }
//     });
//   }

//   stopScan() {
//     return new Promise((resolve) => {
//       BarcodeScanner.stopScan();
//       resolve(true);
//     });
//   }

//   checkVin(vin, addNow = false) {
//     return new Promise(async (resolve) => {
//       if (!vin) {
//         resolve(null);
//         return;
//       } else {
//         const res = await this.network.CheckVin(vin, addNow);
//         console.log('responsive data', res);
//         resolve(res.Data[0].data);
//       }
//     });
//   }

//   askToAddVinVehicle(vin) {
//     return new Promise(async (resolve) => {
//       const actionSheet = await this.actionSheetController.create({
//         header: 'No Match Found...',
//         subHeader: 'Do you want to continue?',
//         cssClass: 'my-custom-class',
//         buttons: [
//           {
//             text: 'Appraisals',
//             icon: 'share',
//             handler: () => {
//               resolve(1);
//             },
//           },
//           {
//             text: 'ADD NOW',
//             icon: 'add-circle',
//             handler: () => {
//               resolve(2);
//             },
//           },
//           {
//             text: 'SKIP',
//             icon: 'close',
//             role: 'cancel',
//             handler: () => {
//               resolve(null);
//             },
//           },
//         ],
//       });
//       await actionSheet.present();

//       // const { role } = await actionSheet.onDidDismiss();
//       // console.log('onDidDismiss resolved with role', role);
//     });
//   }

//   askToLoadVinVehicle(vin) {
//     return new Promise(async (resolve) => {
//       const actionSheet = await this.actionSheetController.create({
//         header: `${vin} Already Exist`,
//         cssClass: 'my-custom-class',
//         buttons: [
//           {
//             text: 'Load Vehicle',
//             icon: 'download-outline',
//             handler: () => {
//               resolve(vin);
//             },
//           },
//           {
//             text: 'Cancel',
//             icon: 'close',
//             role: 'cancel',
//             handler: () => {
//               resolve(null);
//             },
//           },
//         ],
//       });
//       await actionSheet.present();

//       // const { role } = await actionSheet.onDidDismiss();
//       // console.log('onDidDismiss resolved with role', role);
//     });
//   }
// }

// const didUserGrantPermission = async () => {
//   // check if user already granted permission
//   const status = await BarcodeScanner.checkPermission({ force: false });

//   if (status.granted) {
//     // user granted permission
//     return true;
//   }

//   if (status.denied) {
//     // user denied permission
//     return false;
//   }

//   if (status.asked) {
//     // system requested the user for permission during this call
//     // only possible when force set to true
//   }

//   if (status.neverAsked) {
//     // user has not been requested this permission before
//     // it is advised to show the user some sort of prompt
//     // this way you will not waste your only chance to ask for the permission
//     const c = confirm(
//       'We need your permission to use your camera to be able to scan barcodes'
//     );
//     if (!c) {
//       return false;
//     }
//   }

//   if (status.restricted || status.unknown) {
//     // ios only
//     // probably means the permission has been denied
//     return false;
//   }

//   // user has not denied permission
//   // but the user also has not yet granted the permission
//   // so request it
//   const statusRequest = await BarcodeScanner.checkPermission({ force: true });

//   if (statusRequest.asked) {
//     // system requested the user for permission during this call
//     // only possible when force set to true
//   }

//   if (statusRequest.granted) {
//     // the user did grant the permission now
//     return true;
//   }

//   // user did not grant the permission, so he must have declined the request
//   return false;
// };
