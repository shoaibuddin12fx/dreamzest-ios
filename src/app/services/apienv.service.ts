import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApienvService {
  // The values that are defined here are the default values that can
  // be overridden by apiConfig.js

  // API url
  public API_Config = {
    isProd: environment.production,
    // isStage: environment.isStage,
    apiUrl: 'https://mr-98233393.v1-services.spherewms.com/api',
    // authTempUrl: environment.authTempUrl,
    // versionCheckUrl: environment.versionCheckUrl
  };

  // Whether or not to enable debug mode
  public enableDebug = true;

  constructor() {}
}
