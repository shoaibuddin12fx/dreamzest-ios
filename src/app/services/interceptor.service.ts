import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { UsersService } from './users.service';



@Injectable({
  providedIn: 'root' 
})
export class InterceptorService implements HttpInterceptor  {

  constructor(private user:UsersService) { 

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.callToken()).pipe(
      switchMap(token => {
        console.log("Token",{token});
        const cloneRequest = this.addSecret(req, token );
        return next.handle(cloneRequest);
      })
    );
    
  }

  callToken(){
    return new Promise( async resolve => {
      let token = await this.user.getUserToken();
      resolve(token)
    });
  }

  private addSecret(request: HttpRequest<any>, value: any){
    let v = value ? value : '';
    let obj = {
      Authorization: 'Bearer: ' + v,
      Origin: 'invapp.automaticlot.com',
      Referer: 'invapp.automaticlot.com',
    }
    
    
    obj['Accept'] = 'application/json';
    let cnt = request.headers.get('Content-Type');
    if(cnt == 'application/json'){
      obj['Content-Type'] = request.headers.get('Content-Type');
    }

    console.log(obj);
    const clone = request.clone(
      {
        setHeaders: obj
      }
    );

    return clone;
  }
}
