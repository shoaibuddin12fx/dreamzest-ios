import { Injectable } from '@angular/core';
import { global } from '../config/constants';
import { NavService } from './basic/nav.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {


  constructor(public utility: UtilityService, public sqlite: SqliteService, public network: NetworkService, public nav: NavService) { }

  getActiveUser() {
    return this.sqlite.getActiveUser();
  }

  login(formdata) {

    return new Promise(async resolve => {      
      const res = await this.network.login(formdata);
      console.log("userService", res);
      if (res.length > 0) {
        const user = await this.sqlite.setUserInDatabase(res[0]);
        localStorage.setItem('membershipId', res[0].MembershipId)
        // get user rights and save in database
        await this.dumpPermissions();
        resolve(user);
      } else {
        resolve(null);
      }
    })
  }

  getUserToken() {
    return localStorage.getItem('token');
  }

  dumpPermissions() {
    return new Promise(async resolve => {
      const permissions = await this.network.getRights();
      const per_res = await this.sqlite.setPermissionsInDatabase(permissions);
      resolve(per_res);
    })
  }

  logout() {
    // this.menuCtrl.enable(false, 'authenticated');
    this.sqlite.setLogout();
    this.nav.setRoot('pages/login');
  }

}
