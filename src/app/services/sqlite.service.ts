import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { StorageService } from './storage.service';
import { browserDBInstance } from './browser-db-instance';
import { CapacitorSQLite, JsonSQLite } from '@capacitor-community/sqlite';
import { Capacitor } from '@capacitor/core';
// import { CrWriterSqliteService } from '../pages/cr-writer/services/cr-writer-sqlite';

declare var window: any;
const SQL_DB_NAME = '__inv360.db';

@Injectable({
  providedIn: 'root',
})
export class SqliteService {
  db: any;
  sqlite;
  DB_SETUP_KEY = 'first_db_setup';
  batchSqlCmd;
  batchSqlCmdVals;
  public tableNames = ['users', 'premissions', 'Stores', 'SpareTypes', 'Odor', 'TireBrands', 'TireTreads',
    'TireTypes', 'WheelTypes', 'Categories', 'Components', 'Conditions', 'Recommendations', 'Severity'];
  config: any = {
    name: '__inv360.db',
    location: 'default',
  };

  public msg = 'Sync In Progress ...';

  constructor(private storage: StorageService, private platform: Platform) { }

  public initialize() {
    return new Promise(async (resolve) => {
      if (Capacitor.getPlatform() != 'web') {
        await this.initializeDatabase();
        resolve(true);
      } else {
        this.storage.getKey('is_database_initialized').then(async (v) => {
          if (!v) {
            const res = await this.initializeDatabase();
            resolve(res);
          } else {
            resolve(true);
          }
        });
      }
    });
  }

  async initializeDatabase() {
    return new Promise(async (resolve) => {
      await this.platform.ready();
      // initialize database object
      const flag = await this.createDatabase();

      if (!flag) {
        resolve(false);
        return;
      }

      // initialize all tables

      let is_database_initialized = localStorage.getItem('is_database_initialized');

      if (is_database_initialized != "true") {
        // initialize users table
        await this.initializeUsersTable();
        await this.initializePermissionsTable();
        await this.initializeStoreTable();
        await this.initializeAvailabilityTable();
        await this.initializeBodyTypeTable();
        await this.initializeDoorsTable();
        await this.initializeExteriorColorsTable();
        await this.initializeInteriorColorsTable();
        await this.initializeInteriorTypeTable();
        await this.initializeExteriorColorTable();
        await this.initializeInteriorColorTable();
        await this.initializeCostTypeTable();
        await this.initializePayTypeTable();
        await this.initializeStateTable();
        await this.initializeAtttachmentsTable();
        await this.initializeRecentVehiclesTable();
        await this.initializeCrReportTable();
        await this.initializeCrReportConditionTable();


        // initialize loadedCrDdl table
        let tableArray = [
          'SpareTypes',
          'Odor',
          'TireBrands',
          'TireTreads',
          'TireTypes',
          'WheelTypes',
          'Categories',
          'Components',
          'Conditions',
          'Recommendations',
          'Severity',
        ];

        for (var i = 0; i < tableArray.length; i++) {
          await this.initializeCrTable(tableArray[i]);
        }

        localStorage.setItem('is_database_initialized', "true");
      }

      resolve(true);
    });
  }

  async initializeCrTable(table) {
    return new Promise((resolve) => {
      // create statement
      var sql = 'CREATE TABLE IF NOT EXISTS ' + table + '(';
      sql += 'Id INTEGER PRIMARY KEY, ';
      sql += 'Name TEXT, ';
      sql += 'ParentId INTEGER ';
      sql += ')';

      this.msg = 'Initializing ' + table + ' ...';
      resolve(this.execute(sql, []));
    });
  }

  async createDatabase() {
    return new Promise(async (resolve) => {

      if (Capacitor.getPlatform() != 'web') {
        var self = this;
        const dbName = SQL_DB_NAME;
        const dbNames = [SQL_DB_NAME];
        CapacitorSQLite.checkConnectionsConsistency({
          dbNames: dbNames,
        }).then(async (ret) => {

          if (ret.result == false) {
            await CapacitorSQLite.createConnection({ database: dbName });
            await CapacitorSQLite.open({ database: dbName });
            resolve(true);
          } else {
            await CapacitorSQLite.open({ database: dbName });
            resolve(true);
          }

        }).catch(err => {
          // console.log(err)
        });

      } else {
        let _db = window.openDatabase(
          SQL_DB_NAME,
          '1.0',
          'DEV',
          5 * 1024 * 1024
        );
        this.db = browserDBInstance(_db);
        this.msg = 'Database initialized';
        resolve(true);
      }
    });
  }

  async initializeUsersTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = 'CREATE TABLE IF NOT EXISTS users(';
      sql += 'MembershipId TEXT PRIMARY KEY, ';
      sql += 'EmployeeVendorId INTEGER, ';
      sql += 'UserType INTEGER, ';
      sql += 'UserDisplayImage VARCHAR, ';
      sql += 'Email TEXT, ';
      sql += 'FullName TEXT, ';
      sql += 'storeName TEXT, ';
      sql += 'StoreId INTEGER, ';
      sql += 'StoreCode TEXT, ';
      sql += 'GroupId INTEGER, ';
      sql += 'GroupName TEXT, ';
      sql += 'active INTEGER DEFAULT 0, ';
      sql += 'token TEXT ';
      sql += ')';

      this.msg = 'Initializing Users ...';
      resolve(this.execute(sql, []));
    });
  }

  // USERS QUERY STARTS

  public async setUserInDatabase(_user) {
    return new Promise(async (resolve) => {
      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      // console.log(_user);
      var sql = 'INSERT OR REPLACE INTO users(';
      sql += 'MembershipId, ';
      sql += 'EmployeeVendorId, ';
      sql += 'UserType, ';
      sql += 'UserDisplayImage, ';
      sql += 'Email, ';
      sql += 'FullName, ';
      sql += 'storeName, ';
      sql += 'StoreId, ';
      sql += 'StoreCode, ';
      sql += 'GroupId, ';
      sql += 'GroupName ';
      sql += ') ';

      sql += 'VALUES (';

      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '? '; // 11
      sql += ')';

      var values = [
        _user.MembershipId,
        _user.EmployeeVendorId,
        _user.UserType,
        _user.UserDisplayImage,
        _user.Email,
        _user.FullName,
        _user.storeName,
        _user.StoreId,
        _user.StoreCode,
        _user.GroupId,
        _user.GroupName,
      ];

      await this.execute(sql, values);

      if (_user.MembershipId) {
        let sql3 = 'UPDATE users SET active = ?';
        let values3 = [0];
        await this.execute(sql3, values3);

        let sql2 =
          'UPDATE users SET token = ?, active = ? where MembershipId = ?';
        let values2 = [_user.token, 1, _user.MembershipId];

        localStorage.setItem('token', _user.MembershipId);

        await this.execute(sql2, values2);
      }

      resolve(await this.getActiveUser());
    });
  }

  public async getCurrentUserAuthorizationToken() {
    return new Promise(async (resolve) => {
      let user_id = await this.getActiveUserId();
      let sql = 'SELECT token FROM users where id = ? limit 1';
      let values = [user_id];

      let d = await this.execute(sql, values);
      // this.utility.presentToast(d);
      if (!d) {
        resolve(null);
        return;
      }
      // var data = d as any[];
      const data = await this.getRows(d);
      if (data.length > 0) {
        resolve(data[0]['token']);
      } else {
        resolve(null);
      }
    });
  }
  public async getTableDAta() {
    return new Promise(async (resolve) => {
      // let user_id = await this.getActiveUserId();
      for (let i = 0; i < this.tableNames.length; i++) {
        let deleteTableValues = this.tableNames[i];
        let sql = "DELETE FROM " + deleteTableValues;
        const res = await this.execute(sql, []);
      }

      resolve(this.tableNames);
    });
  }

  public async setUserActiveById(id) {
    return new Promise(async (resolve) => {
      let sql3 = 'UPDATE users SET active = ?';
      let values3 = [0];
      await this.execute(sql3, values3);

      let sql2 = 'UPDATE users SET active = ? where id = ?';
      let values2 = [1, id];
      await this.execute(sql2, values2);

      resolve(this.getUserById(id));
    });
  }

  public async setRefreshToken(token, refreshToke) {
    return new Promise(async (resolve) => {
      let sql3 =
        'UPDATE users SET token = ?, refreshToken = ? where active = ?';
      let values3 = [token, refreshToke, 1];
      await this.execute(sql3, values3);

      resolve(true);
    });
  }

  public async getUserById(id) {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM users where id = ?';
      let values = [id];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = await this.getRows(d);
      if (data.length > 0) {
        let id = data[0];
        resolve(id);
      } else {
        resolve(null);
      }
    });
  }

  public async getActiveUserId(): Promise<string> {
    return new Promise(async (resolve) => {

      let mid = localStorage.getItem('membershipId');
      if (mid) {
        resolve(mid)
      } else {
        let sql = 'SELECT MembershipId FROM users where active = ?';
        let values = [1];

        let d = await this.execute(sql, values);
        if (!d) {
          resolve('');
        }
        // var data = d as any[];
        const data = await this.getRows(d);
        if (data.length > 0) {
          let id = data[0]['MembershipId'];
          localStorage.setItem('membershipId', id);
          resolve(id);
        } else {
          resolve('');
        }
      }



    });
  }

  public async getActiveUser() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM users where active = ? ';
      let values = [1];

      let d = await this.execute(sql, values);
      var _data = d as any[];
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        var user = data[0];
        resolve(user);
      } else {
        resolve(null);
      }
    });
  }

  setLogout() {
    return new Promise(async (resolve) => {
      let user_id = await this.getActiveUserId();

      let sql = 'UPDATE users SET token = ?, active = ? where MembershipId = ?';
      let values = [null, 0, user_id];

      localStorage.removeItem('token');

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = await this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }

  // Colors Crud 

  async initializeExteriorColorTable() {
    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS ExteriorColor (";
      sql += "ID INTEGER PRIMARY KEY, ";
      sql += "Name TEXT ";
      sql += ")";

      this.msg = 'Initializing ExteriorColor ...';
      resolve(this.execute(sql, []));
    })
  }

  async initializeInteriorColorTable() {
    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS InteriorColor (";
      sql += "ID INTEGER PRIMARY KEY, ";
      sql += "Name TEXT ";
      sql += ")";

      this.msg = 'Initializing ExteriorColor ...';
      resolve(this.execute(sql, []));
    })
  }

  public async insertExteriorColorTable(stores) {
    return new Promise(async resolve => {

      let insertRows = [];

      for (var i = 0; i < stores.length; i++) {

        var sql = "INSERT OR REPLACE INTO ExteriorColor (";
        sql += "ID, ";
        sql += "Name "
        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "? "
        sql += ")";

        var values = [
          stores[i].ID,
          stores[i].Name
        ];

        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  public async insertInteriorColorTable(stores) {
    return new Promise(async resolve => {

      let insertRows = [];

      for (var i = 0; i < stores.length; i++) {

        var sql = "INSERT OR REPLACE INTO InteriorColor (";
        sql += "ID, ";
        sql += "Name "
        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "? "
        sql += ")";

        var values = [
          stores[i].ID,
          stores[i].Name
        ];

        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  getAllExteriorColor(): Promise<any[]> {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM ExteriorColor';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];

      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  getAllInteriorColor() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM InteriorColor';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      // console.log("All Stores", _data);
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  // End Colors Crud

  // Stores CRUD

  async initializeStoreTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS Stores (";
      sql += "ID INTEGER PRIMARY KEY, ";
      sql += "Name TEXT, ";
      sql += "detail TEXT, ";
      sql += "State TEXT, ";
      sql += "InventoryCount INTEGER, ";
      sql += "Address TEXT, ";
      sql += "ParentID INTEGER, ";
      sql += "MembershipId TEXT, ";
      sql += "active INTEGER ";

      sql += ")";

      this.msg = 'Initializing Stores ...';
      resolve(this.execute(sql, []));
    });
  }

  async initializeRecentVehiclesTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS RecentVehicles (";
      sql += "ID INTEGER PRIMARY KEY, ";
      sql += "VinNumber TEXT, ";
      sql += "StockNo INTEGER, ";
      sql += "Make TEXT, ";
      sql += "Model TEXT, ";
      sql += "Year TEXT, ";
      sql += "StoreId INTEGER ";
      sql += ")";

      this.msg = 'Initializing Stores ...';
      resolve(this.execute(sql, []));
    });
  }
  
  async initializeCrReportTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS CrReport (";
      sql += "ID INTEGER PRIMARY KEY, ";
      sql += "ROId INTEGER, ";
      sql += "Vin TEXT, ";
      sql += "CombinedYearMakeModel TEXT, ";
      sql += "InventoryStoreId INTEGER, ";
      sql += "MilesIn TEXT, ";
      sql += "CRReportComponent TEXT, ";
      sql += "CRReportRecommendation TEXT, ";
      sql += "ExteriorColor TEXT, ";
      sql += "CRReportCondition TEXT, ";
      sql += "CRReportSeverity TEXT ";
      sql += ")";

      this.msg = 'Initializing Stores ...';
      resolve(this.execute(sql, []));
    });
  }
  async initializeCrReportConditionTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS CrReport (";
      sql += "ID INTEGER PRIMARY KEY, ";
      sql += "ROId INTEGER, ";
      sql += "Vin TEXT, ";
      sql += "CombinedYearMakeModel TEXT, ";
      sql += "InventoryStoreId INTEGER, ";
      sql += "MilesIn TEXT, ";
      sql += "CRReportComponent TEXT, ";
      sql += "CRReportRecommendation TEXT, ";
      sql += "ExteriorColor TEXT, ";
      sql += "CRReportCondition TEXT, ";
      sql += "CRReportSeverity TEXT ";
      sql += ")";

      this.msg = 'Initializing Stores ...';
      resolve(this.execute(sql, []));
    });
  }

  async initializeAvailabilityTable() {
    return new Promise(resolve => {
      var sql = "CREATE TABLE IF NOT EXISTS Availability(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";
      resolve(this.execute(sql, []));
    })
  }

  async initializeBodyTypeTable() {
    return new Promise((resolve) => {
      var sql = "CREATE TABLE IF NOT EXISTS BodyType(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";
      resolve(this.execute(sql, []));
    })
  }

  async initializeDoorsTable() {
    return new Promise((resolve) => {

      var sql = "CREATE TABLE IF NOT EXISTS Doors(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";

      resolve(this.execute(sql, []));
    })
  }

  async initializeExteriorColorsTable() {
    return new Promise((resolve) => {
      var sql = "CREATE TABLE IF NOT EXISTS Exterior_Colors(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";
      resolve(this.execute(sql, []));
    })
  }

  async initializeInteriorColorsTable() {
    return new Promise((resolve) => {
      var sql = "CREATE TABLE IF NOT EXISTS Interior_Colors(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";
      resolve(this.execute(sql, []));
    })
  }

  async initializeInteriorTypeTable() {
    return new Promise((resolve) => {
      var sql = "CREATE TABLE IF NOT EXISTS Interior_Type(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";
      resolve(this.execute(sql, []));
    })
  }

  async initializeCostTypeTable() {
    return new Promise((resolve) => {

      var sql = "CREATE TABLE IF NOT EXISTS CostType(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";

      resolve(this.execute(sql, []));
    })
  }





  // set cost types 

  public async insertCostTypeTable(data) {
    return new Promise(async resolve => {

      let memId = await this.getActiveUserId();

      let insertRows = [];

      for (var i = 0; i < data.length; i++) {

        var sql = "INSERT OR REPLACE INTO CostType (";
        sql += "ID, ";
        sql += "Name, "
        sql += "ParentID,"
        sql += "detail"

        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "? "
        sql += ")";

        var values = [
          data[i].ID,
          data[i].Name,
          data[i].ParentID,
          data[i].detail
        ];

        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  // get cost types 
  getAllCostType() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM CostType';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }
  public async insertCrReportTable(data) {
  
  
      console.log("sqliteCrData",data);
      return new Promise(async resolve => {
        let insertRows = [];
  
        for (var i = 0; i < data.length; i++) {

          var sql = "INSERT OR REPLACE INTO CrReport (";
          sql += "ID, ";
          sql += "ROId, "
          sql += "Vin, "
          sql += "CombinedYearMakeModel, "
          sql += "InventoryStoreId, "
          sql += "MilesIn, "
          sql += "CRReportComponent, "
          sql += "CRReportRecommendation, "
          sql += "ExteriorColor, "
          sql += "CRReportCondition, "
          sql += "CRReportSeverity "
          
          sql += ") ";
  
          sql += "VALUES (";
  
          sql += "?, "
          sql += "?, "
          sql += "?, "
          sql += "?, "
          sql += "?, "
          sql += "?, "
          sql += "?, "
          sql += "?, "
          sql += "?, "
          sql += "?, "
          sql += "? "
          sql += ")";
  
          var values = [
            data[i].InventoryId,
            data[i].ROId,
            data[i].Vin,
            data[i].CombinedYearMakeModel,
            data[i].InventoryStoreId,
            data[i].MilesIn,
            data[i].CRReportComponent,
            data[i].CRReportRecommendation,
            data[i].ExteriorColor,
            data[i].CRReportCondition,
            data[i].CRReportSeverity,
          ];
          insertRows.push([sql, values]);
        }
   console.log("rows here",insertRows)
        await this.prepareBatch(insertRows);  
  
        console.log(insertRows)
        resolve(true);
      });
    }
  
    getCrReport(offset) {
      return new Promise(async (resolve) => {
        let sql = 'SELECT * FROM CrReport ';
        let values = [];

        sql += " ORDER BY Vin ASC limit ? OFFSET ?"
        values.push(10, offset);
  console.log("values",values)
        let d = await this.execute(sql, values);
        if (!d) {
          let obj = {
            offset: -1,
            list: []
          }
          resolve(obj);
          return;
        }

  
        var _data = d as any[];
        const data = await this.getRows(_data);
        if (data && data.length > 0) {

          offset = (data.length < 10) ? -1 : (offset + 10)

          let obj = {
            offset: offset,
            list: data
          }
          console.log("got list here",obj)
          resolve(obj);
          return;
        } else {

          let obj = {
            offset: -1,
            list: []
          }
          resolve(obj);
          return;
        }
      });
    }


public async insertRecentVehiclesTable(data) {


    console.log(data);
    return new Promise(async resolve => {
        var sql = "INSERT OR REPLACE INTO RecentVehicles (";
        sql += "ID, ";
        sql += "VinNumber, "
        sql += "StockNo, "
        sql += "Make, "
        sql += "Model, "
        sql += "Year, "
        sql += "StoreId "
        
        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "? "
        sql += ")";

        var values = [
          data.id,
          data.vin,
          data.stockNo,
          data.make,
          data.model,
          data.year,
          data.storeId
        ];


      await this.execute(sql, values);  


      resolve(true);
    });
  }

  getRecentVehicles() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM RecentVehicles';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  
  deleteRecentVehicles() {
    return new Promise(async (resolve) => {
      let sql = 'DELETE FROM RecentVehicles';
      let values = [];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = await this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }


  async initializePayTypeTable() {
    return new Promise((resolve) => {

      var sql = "CREATE TABLE IF NOT EXISTS PayType(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";

      resolve(this.execute(sql, []));
    })
  }

  // set pay type
  public async insertPayTypeTable(data) {
    return new Promise(async resolve => {

      let memId = await this.getActiveUserId();

      let insertRows = [];

      for (var i = 0; i < data.length; i++) {

        var sql = "INSERT OR REPLACE INTO PayType (";
        sql += "ID, ";
        sql += "Name, "
        sql += "ParentID,"
        sql += "detail"

        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "? "
        sql += ")";

        var values = [
          data[i].ID,
          data[i].Name,
          data[i].ParentID,
          data[i].detail
        ];

        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  getAllPayType() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM PayType';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  public async insertStoreTable(stores) {
    return new Promise(async resolve => {

      let memId = await this.getActiveUserId();

      let insertRows = [];

      for (var i = 0; i < stores.length; i++) {

        var sql = "INSERT OR REPLACE INTO Stores (";
        sql += "ID, ";
        sql += "Name, "
        sql += "detail, "
        sql += "State, "
        sql += "InventoryCount, "
        sql += "Address,"
        sql += "ParentID,"
        sql += "MembershipId, "
        sql += "active"
        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "?"
        sql += ")";

        var values = [
          stores[i].ID,
          stores[i].Name,
          stores[i].detail,
          stores[i].State,
          stores[i].InventoryCount,
          stores[i].Address,
          stores[i].ParentID,
          memId,
          stores[i].ID == 13 ? 1 : 0
        ];

        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }


  getAllStores() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM Stores';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      // console.log("All Stores", _data);
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  // Initialize States
  async initializeStateTable() {
    return new Promise((resolve) => {

      var sql = "CREATE TABLE IF NOT EXISTS states(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";

      resolve(this.execute(sql, []));
    })
  }

  // Insert States
  public async insertStateTable(states) {
    return new Promise(async resolve => {



      let insertRows = [];

      for (var i = 0; i < states.length; i++) {

        var sql = "INSERT OR REPLACE INTO states (";
        sql += "ID, ";
        sql += "Name, "
        sql += "ParentID,"
        sql += "detail"

        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "? "
        sql += ")";

        var values = [
          states[i].ID,
          states[i].Name,
          states[i].ParentID,
          states[i].detail
        ];

        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }


  // Get States
  getAllStates() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM states';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      const data = await this.getRows(_data);
      // console.log("All States", data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }
  // Initialize Attachments
  async initializeAtttachmentsTable() {
    return new Promise((resolve) => {

      var sql = "CREATE TABLE IF NOT EXISTS states(";
      sql += "ID INTEGER PRIMARY KEY,";
      sql += "Name TEXT,"
      sql += "ParentID INTEGER,"
      sql += "detail TEXT"
      sql += ")";

      resolve(this.execute(sql, []));
    })
  }

  // Insert Attachments
  public async insertAtttachmentsTable(states) {
    return new Promise(async resolve => {



      let insertRows = [];

      for (var i = 0; i < states.length; i++) {

        var sql = "INSERT OR REPLACE INTO states (";
        sql += "ID, ";
        sql += "Name, "
        sql += "ParentID,"
        sql += "detail"

        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "?, "
        sql += "?, "
        sql += "? "
        sql += ")";

        var values = [
          states[i].ID,
          states[i].Name,
          states[i].ParentID,
          states[i].detail
        ];

        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  // Get States
  getAllAttachments() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM states';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      const data = await this.getRows(_data);
      // console.log("All States", data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }



  getAllAvailability() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM Availability';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      // console.log("All Availability", _data);
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }


  getAllBodyType() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM BodyType';
      let values = [];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      // console.log("All BodyType", _data);
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  getDefaultStoresOfUser() {
    return new Promise(async resolve => {

      let memId = await this.getActiveUserId();


      let sql = "SELECT * FROM Stores where MembershipId = ? and active = ?";
      let values = [memId, 1];

      let d = await this.execute(sql, values);

      var _data = d as any[];
      // console.log(_data);
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    })

  }

  setDefaultStoresOfUser(id) {

    return new Promise(async (resolve) => {

      let memId = await this.getActiveUserId();

      let sql3 = 'UPDATE Stores SET active = ? where MembershipId = ?';
      let values3 = [0, memId];
      await this.execute(sql3, values3);

      let sql2 = 'UPDATE Stores SET active = ? where id = ? and MembershipId = ?';
      let values2 = [1, id, memId];
      await this.execute(sql2, values2);

      resolve(true);
    });

  }



  // Permissions CRUD STARTS

  async initializePermissionsTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = 'CREATE TABLE IF NOT EXISTS permissions(';
      sql += 'MembershipId TEXT, ';
      sql += 'PermissionName TEXT ';
      sql += ')';

      this.msg = 'Initializing permissions ...';
      resolve(this.execute(sql, []));
    });
  }

  public async setPermissionsInDatabase(_permissions) {
    return new Promise(async (resolve) => {
      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      let mid = await this.getActiveUserId();

      if (!mid) {
        resolve(null);
        return;
      }

      await this.deleteAllPermissions(mid);

      let insertRows = [];

      for (var i = 0; i < _permissions.length; i++) {
        var sql = 'INSERT OR REPLACE INTO permissions(';
        sql += 'MembershipId, ';
        sql += 'PermissionName ';
        sql += ') ';

        sql += 'VALUES (';

        sql += '?, ';
        sql += '? ';
        sql += ')';

        var values = [mid, _permissions[i].PermissionName];

        // await this.execute(sql, values);
        insertRows.push([sql, values]);
      }

      await this.prepareBatch(insertRows);

      resolve(true);
    });
  }

  deleteAllPermissions(mid) {
    return new Promise(async (resolve) => {
      let sql = 'DELETE from permissions where MembershipId = ?';
      let values = [mid];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = await this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }

  getAllPermissions() {
    return new Promise(async (resolve) => {
      let sql = 'SELECT * FROM permissions';
      let values = [];

      let d = await this.execute(sql, values);
      var _data = d as any[];
      const data = await this.getRows(_data);
      if (data && data.length > 0) {
        var permissions = data;
        resolve(permissions);
      } else {
        resolve(null);
      }
    });
  }

  // Permissions CRUD END

  execute(sql, params) {
    return new Promise(async (resolve) => {
      if (Capacitor.getPlatform() != 'web') {
        await this.createDatabase();
      } else {
        if (!this.db) {
          await this.platform.ready();
          // initialize database object
          await this.createDatabase();
        }
      }
      // console.log("queries", sql);
      // // if(this.platform.is('cordova')){
      // console.log(params);

      if (Capacitor.getPlatform() != 'web') {
        CapacitorSQLite.query({
          database: SQL_DB_NAME,
          statement: sql,
          values: params,
        })
          .then((response) => {
            resolve(response);
          })
          .catch((err) => {
            console.error(err);
            resolve(null);
          });
      } else {
        this.db
          .executeSql(sql, params)
          .then((response) => {

            if (response.rows) {
              response['values'] = response.rows;
            }
            resolve(response);
          })
          .catch((err) => {
            console.error(err);
            resolve(null);
          });
      }
    });
  }

  prepareBatch(insertRows) {
    return new Promise(async (resolve) => {
      var size = 250;
      var arrayOfArrays = [];

      for (var i = 0; i < insertRows.length; i += size) {
        arrayOfArrays.push(insertRows.slice(i, i + size));
      }

      // console.log(arrayOfArrays);

      for (var j = 0; j < arrayOfArrays.length; j++) {
        await this.executeBatch(arrayOfArrays[j]);
        // await this.execute(s, p)
      }

      resolve(true);
    });
  }

  executeBatch(array) {
    return new Promise(async (resolve) => {
      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase();
      }

      let command = array[0][0];

      if (!command) {
        resolve(null);
      }

      let cmd = command.split('VALUES')[0] + 'VALUES ';
      let values = [];

      for (let i = 0; i < array.length; i++) {
        let extractedArray = array[i];
        let brackets = array[i][0].split('VALUES')[1];
        cmd += brackets + (i != array.length - 1 ? ', ' : '');
        values = values.concat(array[i][1]);
      }
      // console.log("batch sql cmd: ",cmd);
      // console.log("batch sql cmd 00 : ",values);

      return CapacitorSQLite.run({
        database: SQL_DB_NAME,
        statement: cmd,
        values: values,
      })
        .then((response) => {
          // console.log("Response:", {response});
          resolve(response);
        })
        .catch((err) => {
          console.error(err);
          resolve(null);
        });
    });
  }

  private setValue(k, v) {
    return new Promise((resolve) => {
      this.storage.setKey(k, v).then(() => {
        resolve({ k: v });
      });
    });
  }

  private getValue(k): Promise<any> {
    return new Promise((resolve) => {
      this.storage.getKey(k).then((r) => {
        resolve(r);
      });
    });
  }

  public getRows(data): Promise<any[]> {
    return new Promise((resolve) => {
      var items = [];
      for (let i = 0; i < data.values.length; i++) {
        let item = data.values[i];
        items.push(item);
      }
      resolve(items);
    });
  }

}
