import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { global } from '../config/constants';
import { ApiService } from './api.service';
// import { CheckInParams } from '../models/check-in-list';
// import { EnAddLater } from '../models/en-add-later';
// import { EnDetailingFilter } from '../models/en-detailing-filter';
// import { EnInventoryCostForm } from '../models/EnInventoryCostForm';
// import { InvParams } from '../models/inv-params';
// import { ApiService } from './api.service';
// import { GeolocationsService } from './geolocations.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root',
})
export class NetworkService {
  getAllStates() {
    throw new Error('Method not implemented.');
  }
  ongoing = false;

  constructor(
    public utility: UtilityService,
    public api: ApiService,
    public sqlite: SqliteService // public geolocation: GeolocationsService
  ) {}

  login(params) {
    const str = this.serialize(params);
    const res = this.httpPostResponse('Login' + '?' + str, null, false, false);
    console.log('Login Network', res);
    console.log('str Network', str);
    return res;
  }

  subscribe(data) {
    const res = this.httpPostResponse(
      global.API_SUBSCRIBE,
      data,
      false,
      false,
      true
    );
    return res;
  }

  getKeywords(subscription_id, page = 1) {
    let params = {
      subscription_id,
      page,
    };
    const str = this.serialize(params);
    const res = this.httpGetResponse(
      global.API_KEYWORD + '?' + str,
      null,
      false,
      false
    );
    return res;
  }

  getinterpretation(subscription_id, id) {
    let params = {
      subscription_id,
    };
    const str = this.serialize(params);
    const res = this.httpGetResponse(
      global.API_INTERPRETATION + id + '?' + str,
      null,
      false,
      false
    );
    return res;
  }

  searchKeywords(subscription_id, page = 1, search) {
    let params = {
      subscription_id,
      page,
      search,
    };

    const str = this.serialize(params);
    const res = this.httpGetResponse(
      global.API_KEYWORD_SEARCH + '?' + str,
      null,
      false,
      false
    );
    return res;
  }

  subscribeApple(data) {
    const res = this.httpPostResponse(
      global.API_SUBSCRIBE_APPLE,
      data,
      false,
      false,
      true
    );
    return res;
  }

  // async checkVin(vin, addNow = false, loader = false) {
  //   // added params
  //   let mid = await this.sqlite.getActiveUserId();
  //   let params = {};
  //   params['membershipId'] = mid;
  //   params['vinnumber'] = vin;
  //   params['storeId'] = 13;
  //   params['IsAddNow'] = addNow;

  //   const str = this.serialize(params);
  //   return this.httpGetResponse(
  //     'GetBasicTabDataFromVinWithDDL' + '?' + str,
  //     null,
  //     loader
  //   );
  // }

  async insertAddUpdateLaterData(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    console.log(data);

    return this.httpPostResponse(
      'AddUpdateInventoryLater' + '?' + str,
      data,
      false,
      loader,
      true
    );
  }
  //   class func InsertAddUpdateLaterData(_ data: EnAddLater, completion: @escaping (_ callback: ResDataWithoutPDF?) -> Void){

  //     // Post Model Create
  //     guard let mid = UserDataHelper.returnUser().membershipId else { return }

  //     var post = "?";
  //     post += "membershipId=\(mid)";

  //     let postdata = data.toDictionary() as! [String:Any];

  //     //Network Request
  //     NetworkHelper.MakePostRequestWithStringData(api.AddUpdateInventoryLater + post, postData: postdata, showLoader: true, success: { (successData) -> Void in

  //         print(successData!);
  //         let data = ResDataWithoutPDF(successData!);
  //         completion(data)

  //     },failure: { (error) -> Void in

  //         print(error?.localizedDescription ?? "ERROR");
  //         completion(nil);

  //     })

  // }
  async getRights(params = {}) {
    // added params
    let mid = await this.sqlite.getActiveUserId();
    params['membershipId'] = mid;

    const str = this.serialize(params);
    return this.httpGetResponse('GetRights' + '?' + str, null, false, false);
  }

  async getStores(params = {}) {
    // added params
    let mid = await this.sqlite.getActiveUserId();
    params['membershipId'] = mid;

    const str = this.serialize(params);
    return this.httpGetResponse('GetStores' + '?' + str, null, false, false);
  }

  async inventoryList(data, loader = false) {
    // added params
    let mid = await this.sqlite.getActiveUserId();
    let params = {}; //Object.assign({}, data);

    params['membershipId'] = mid;
    params['Page'] = data['Page'] ? data['Page'] : 0;
    params['Size'] = data['Size'] ? data['Size'] : 10;
    params['IsPrimary'] = true;
    params['IsInternal'] = false;
    params['IsCR'] = true;
    params['IsSubInv'] = true;
    params['ischeckIn'] = data['ischeckIn'] ? data['ischeckIn'] : 0;
    params['VinNumber'] = data['VinNumber'] ? data['VinNumber'] : '';
    params['storeid'] = data['storeid'];
    params['ispdf'] = data['ispdf'] ? data['ispdf'] : false;
    params['filterPercentage'] = data['filterPercentage']
      ? data['filterPercentage']
      : 0;

    const str = this.serialize(params);
    return this.httpPostResponse(
      'GetInventoryBySort_BySP' + '?' + str,
      data,
      false,
      loader
    );
  }

  async GetAddLaterData(data: any = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['Page'] = data.pageId;
    params['Size'] = data.sizeId;
    if (data.storeId != -1) {
      params['StoreId'] = data.storeId;
    }
    const str = this.serialize(params);

    return this.httpGetResponse('GetInventoryLater' + '?' + str, null, loader);
  }

  async getEmployeesDDL(storeId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    if (storeId != 0) {
      params['storeId'] = storeId;
    }
    const str = this.serialize(params);

    return this.httpGetResponse('GetEmployees' + '?' + str, null, loader);
  }

  async getVendorDDL(storeId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['storeId'] = storeId;
    const str = this.serialize(params);

    return this.httpGetResponse('GetVendors' + '?' + str, null, loader);
  }

  async GetCostTypeDDL(storeId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['storeId'] = storeId;
    const str = this.serialize(params);

    return this.httpGetResponse('GetCostTabDDL' + '?' + str, null, loader);
  }

  async getAuditCountByStore(storeId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['storeId'] = storeId;
    console.log(params);
    const str = this.serialize(params);
    return this.httpGetResponse(
      'GetAuditCountByStore' + '?' + str,
      null,
      loader
    );
  }

  async getAllKeyAudit(auditId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['auditSummaryId'] = auditId;
    const str = this.serialize(params);

    return this.httpGetResponse('GetAllKeyAudit' + '?' + str, null, loader);
  }
  async getAuditSummaryById(auditId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['auditSummaryId'] = auditId;
    console.log(params);
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetAuditSummaryById' + '?' + str,
      null,
      loader
    );
  }

  async getBasicTabDataFromVinWithDDL(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['VinNumber'] = data.vin;
    console.log(params);
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetBasicTabDataFromVinWithDDL' + '?' + str,
      null,
      loader
    );
  }

  async createAuditSummary(storeId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['storeId'] = storeId;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'CreateAuditSummary' + '?' + str,
      null,
      null,
      loader
    );
  }

  async exportTempList(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    console.log('recent Vehicle', data);
    const str = this.serialize(params);
    return this.httpPostResponse(
      'ExportTempList' + '?' + str,
      data,
      null,
      loader
    );
  }

  async getInventoryCRReport(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'GetInventoryCRReport' + '?' + str,
      data,
      null,
      true
    );
  }

  async createKeyAudit(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    const str = this.serialize(params);
    console.log(params);
    return this.httpPostResponse(
      'CreateKeyAudit' + '?' + str,
      data,
      null,
      loader,
      true,
      'application/json',
      true
    );
  }

  async completeAudit(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['auditSummaryId'] = data;
    console.log(params);
    const str = this.serialize(params);
    console.log(params);
    return this.httpPostResponse(
      'CompleteAudit' + '?' + str,
      null,
      null,
      loader,
      true
    );
  }

  async getInventoryDetailing(data, loader = false) {
    // added params
    let mid = await this.sqlite.getActiveUserId();
    let params = {}; // Object.assign({}, data);

    params['membershipId'] = mid;
    params['Page'] = data.Page;
    params['Size'] = data.Size;
    console.log(params);
    const str = this.serialize(params);
    return this.httpGetResponse(
      'GetInventoryDetailing' + '?' + str,
      data,
      false,
      loader
    );
  }

  async getInventoryId(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['vinnumber'] = data.VinNumber;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetInventoryId' + '?' + str,
      null,
      null,
      loader
    );
  }

  async getVinScanReport(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['id'] = data.id;
    params['membershipId'] = mid;
    params['printtype'] = data.vin;
    const str = this.serialize(params);

    return this.httpGetResponse('GetVinScanReport' + '?' + str, null, loader);
  }

  async getAllKeyAuditSummary(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['storeId'] = data;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetAllKeyAuditSummary' + '?' + str,
      null,
      loader
    );
  }

  async exteriorInventoryDDl(loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'Exterior_InventoryDDl' + '?' + str,
      null,
      null,
      loader
    );
  }

  async getCategoryDDL(loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'ComponentCategory' + '?' + str,
      null,
      null,
      loader
    );
  }
  async updateEmployeePicture(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    console.log(data);
    return this.httpPostResponse(
      'UpdateEmployeePicture' + '?' + str,
      data,
      null,
      loader
    );
  }

  async addUpdateVendor(loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'AddUpdateVendor' + '?' + str,
      null,
      null,
      loader
    );
  }

  async sortByOptionsForInventory(screenName, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['ScreenName'] = screenName;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetSortByOptionForInventory' + '?' + str,
      null,
      loader
    );
  }

  //   class func GetAddLaterData(page:Int,storeid:Int, completion: @escaping (_ callback: [EnAddLater]?) -> Void, _ size:Int = 30){

  //     // Post Model Create
  //     guard let mid = UserDataHelper.returnUser().membershipId else { return }

  //     var post = "?";
  //     post += "membershipId=\(mid)&";
  //     post += "Page=\(page)&";
  //     post += "Size=\(size)&";
  //     if(storeid != -1){
  //         post += "StoreId=\(storeid)";
  //     }

  //     //Network Request
  //     NetworkHelper.MakeGetRequest(api.GetAddLaterData + post, postData: nil, showLoader: true, success: { (successData) -> Void in

  //         print(successData!);
  //         let data = successData! as [NSDictionary];
  //         guard let AddLaterData = [EnAddLater](dictionaryArray: data) as? [EnAddLater] else { completion(nil) };
  //         UtilityHelper.HideLoader();
  //         completion(AddLaterData)

  //     },failure: { (error) -> Void in

  //         print(error?.localizedDescription ?? "ERROR");
  //         completion(nil);

  //     })

  // }

  async GetInventoryDetailingDDL(loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetInventoryDetailingDDL' + '?' + str,
      null,
      loader
    );
  }

  async getInventoryInfo(vin, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;
    params['vinnumber'] = vin;
    const str = this.serialize(params);

    return this.httpGetResponse('GetInventoryId' + '?' + str, null, loader);
  }

  async getDetailingData(data = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetInventoryDetailing' + '?' + str,
      null,
      loader
    );
  }

  async getROListDataForMultiple(data = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    let FilterData = params['FilterData'];
    delete params['FilterData'];
    const str = this.serialize(params);

    return this.httpPostResponse(
      'GetManageROList' + '?' + str,
      FilterData,
      null,
      loader
    );
  }
  // Delete Detailing Data DeleteInventoryDetailing
  async deleteInventoryDetailing(id, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['id'] = id;
    const str = this.serialize(params);
    return this.httpDeleteResponse(
      'DeleteInventoryDetailing' + '?' + str,
      null,
      loader
    );
  }

  // INVENTORY OPTIONS
  async getGetInvOptions(invId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['InventoryId'] = invId;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetInventoryOptions' + '?' + str,
      null,
      loader
    );
  }

  // Inventory Valuations
  async callValutionApi(invId, loader = false): Promise<any[]> {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['InventoryId'] = invId;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetInventoryValution' + '?' + str,
      null,
      loader
    );
  }

  // For Vendors
  async getVendorAgainstCostID(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetVendorListByExpenseType' + '?' + str,
      null,
      loader
    );
  }

  async getTitleDataHistory(data = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetInventoryTitleHistory' + '?' + str,
      null,
      loader
    );
  }

  async getTitleDataApi(data = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetInventoryTitleLog' + '?' + str,
      null,
      loader
    );
  }
  // Add Details detailing/add-details
  async insertDetailing(data = {}, loader = false) {
    console.log('got data', { data });
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'InsertInventoryDetailing' + '?' + str,
      data,
      null,
      loader
    );
  }

  // Update Inventory Detailing
  async UpdateInventoryDetailingCompleteStatus(data, loader = false) {
    console.log('got update data', { data });
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'UpdateInventoryDetailingCompleteStatus' + '?' + str,
      {},
      null,
      loader,
      true
    );
  }
  async getTitleAttachmentPictures(data: any = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {}; // Object.assign({}, data);
    params['membershipId'] = mid;
    params['inventory'] = data.inventory;
    params['page'] = 0;
    params['size'] = 1;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetTitleAttachmentPictures' + '?' + str,
      null,
      loader
    );
  }

  async insertBasicTabData(data = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'SaveBasicTabData' + '?' + str,
      data,
      null,
      loader
    );
  }
  async getROforInventory(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['Page'] = data.page;
    params['Size'] = data.size;
    params['ispdf'] = data.ispdf;
    const str = this.serialize(params);
    console.log(str);
    return this.httpPostResponse(
      'GetROforInventory' + '?' + str,
      data,
      null,
      loader
    );
  }

  async getCostData(invId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['InventoryId'] = invId;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetAllCostInvoicesByInventoryId' + '?' + str,
      null,
      loader
    );
  }

  async getVehicleSourceType(loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetVehicleSourceType' + '?' + str,
      null,
      loader
    );
  }

  async getStatusDDL(loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({});
    params['membershipId'] = mid;

    const str = this.serialize(params);

    return this.httpGetResponse('GetStatusDDL' + '?' + str, null, loader);
  }

  async getAuditList(data = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    let FilterData = params['FilterData'];
    delete params['FilterData'];
    const str = this.serialize(params);

    // recheck this one
    return this.httpGetResponse(
      'GetInventoryAudit' + '?' + str,
      FilterData,
      null,
      loader
    );
  }

  async getCrDDL(loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpGetResponse('GetCRDDL' + '?' + str, null, loader);
  }

  async getCrDetail(invId, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['InventoryId'] = invId;
    const str = this.serialize(params);

    return this.httpGetResponse('GetCRDetail' + '?' + str, null, loader);
  }

  async addUpdateCRDetail(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'AddUpdateCRDetail' + '?' + str,
      data,
      null,
      loader
    );
  }

  // Insert Valuable Options
  async insertValution(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'InsertValution' + '?' + str,
      data,
      null,
      loader
    );
  }

  async insertRONotes(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'InsertRONotes' + '?' + str,
      data,
      null,
      loader,
      true
    );
  }
  async insertInventoryCostForm(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'InsertInventoryCostForm' + '?' + str,
      data,
      null,
      loader
    );
  }

  async insertOptionsTabData(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'InsertInventoryOptions' + '?' + str,
      data,
      null,
      loader
    );
  }

  async deleteCRRO(id, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['Id'] = id;
    const str = this.serialize(params);

    return this.httpPostResponse('DeleteCRRO' + '?' + str, null, null, loader);
  }

  async UploadCrComponentImageLinkToCr(id, postData, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['InventoryId'] = id;
    let paramData = {
      imageData: postData.get('imageData'),
      imageName: postData.get('imageName'),
    };
    const str = this.serialize(params);
    return this.httpPostResponse(
      'AddCrAttachment' + '?' + str,
      { attachments: postData },
      null,
      loader,
      null,
      'multipart/form-data'
    );
  }

  async addUpdateTire(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'AddUpdateTire' + '?' + str,
      data,
      null,
      loader
    );
  }

  // check in out apis
  async getInventoryCheckInDetails(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpGetResponse('GetInventory' + '?' + str, null, loader);
  }

  // Get Valuable Options
  async getInventoryValution(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['InventoryId'] = data.invId;
    console.log({ params });
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetInventoryValution' + '?' + str,
      null,
      loader
    );
  }

  async getBasicTabData(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['VinNumber'] = data.Vin;
    params['InventoryId'] = data.InvID;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetBasicTabDataWithDDL' + '?' + str,
      null,
      loader
    );
  }

  async getAuditHistory(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['InventoryId'] = data.invID;
    params['Page'] = data.page;
    params['Size'] = data.Size;
    const str = this.serialize(params);
    return this.httpGetResponse(
      'GetInventoryAuditHistory' + '?' + str,
      null,
      loader
    );
  }

  async getDashboardData(data: any) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['storeId'] = data.storeId;
    const str = this.serialize(params);
    return this.httpGetResponse(
      'GetInventoryDashboardCounts' + '?' + str,
      null
    );
  }

  // Appraisals
  async getColors() {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpGetResponse(
      'GetColors' + '?' + str,
      null,
      false,
      false,
      'application/json',
      true
    );
  }

  async getStates() {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse('State_DDl' + '?' + str, null);
  }

  GetStateDDL;

  async getAuditReportData(data: any, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['Page'] = data.page;
    params['Size'] = data.size;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'GetInventoryScannedByDate' + '?' + str,
      data,
      null,
      loader
    );
  }

  async getInsertInvCostForm(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'InsertInventoryCostForm' + '?' + str,
      data,
      null,
      loader
    );
  }

  async InsertPostTitleData(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    console.log('got data', { data });
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'InsertInventoryTitleLogPost' + '?' + str,
      data,
      null,
      loader
    );
  }

  async insertPreTitleData(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'InsertInventoryTitleLogPre' + '?' + str,
      data,
      null,
      loader
    );
  }

  async updateInventoryDetailing(data, loader = true) {
    console.log('got data here', { data });
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'UpdateInventoryDetailing' + '?' + str,
      data,
      null,
      loader,
      true
    );
  }

  async getManageROList(data = {}, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['Page'] = params['page'];
    params['SourceId'] = params['sourceId'];
    params['Size'] = params['size'];
    params['Vin'] = params['vin'];
    params['Storedid'] = params['storeId'];
    let FilterData = params['FilterData'];
    delete params['FilterData'];
    const str = this.serialize(params);

    return this.httpPostResponse(
      'GetManageROList' + '?' + str,
      FilterData,
      null,
      loader
    );
  }

  // async getManageROList(data, loader = true) {
  //   console.log('got data here', data);
  //   let mid = await this.sqlite.getActiveUserId();
  //   let params = {};
  //   params['membershipId'] = mid;
  //   params['Page'] = data.page;
  //   params['SourceId'] = data.sourceId;
  //   params['Size'] = data.size;
  //   params['Vin'] = data.vin;
  //   params['Storedid'] = data.storeId;
  //   const str = this.serialize(params);
  //   console.log("filters",JSON.stringify(data.Data))
  //   return this.httpPostResponse(
  //     'GetManageROList' + '?' + str,
  //     JSON.stringify(data.Data),
  //     null,
  //     loader,
  //     false,
  //     null,
  //     true
  //   );
  // }

  async getRoDEtailsForInventory(invId, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['InventoryID'] = invId;
    const str = this.serialize(params);
    return this.httpGetResponse(
      'GetRoDEtailsForInventory' + '?' + str,
      null,
      loader,
      true
    );
  }

  async addCustomTags(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['customtag'] = data.tag;
    params['InvId'] = data.id;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'AddCustomTags' + '?' + str,
      data,
      null,
      loader,
      false
    );
  }

  async fileAttachment(loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['inventory'] = 744;
    params['page'] = 0;
    params['size'] = 1000;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'FileAttachment' + '?' + str,
      null,
      null,
      loader,
      false
    );
  }

  async insertInventoryAttachmentPictures(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'InsertInventoryAttachmentPictures' + '?' + str,
      data,
      null,
      loader,
      false
    );
  }

  async getInventoryCustomTags(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['groupId'] = 1;
    params['storeId'] = data.storeId;
    params['userId'] = 1;
    params['InvId'] = data.id;
    const str = this.serialize(params);
    return this.httpGetResponse(
      'GetInventoryCustomTag' + '?' + str,
      null,
      loader,
      false
    );
  }

  async deleteCustomTags(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['customtag'] = data.TagName;
    params['InvId'] = data.InventoryId;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'DeleteCustomTags' + '?' + str,
      data,
      null,
      loader,
      false
    );
  }

  async GetRODetailData(roID, loader = true) {
    let mid = await this.sqlite.getActiveUserId();

    let params = {};

    params['membershipId'] = mid;
    params['roID'] = roID;

    const str = this.serialize(params);

    return this.httpGetResponse('GetRODetails' + '?' + str, null, loader);
  }

  async SaveEstimation(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();

    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['roId'] = data.roId;
    params['estTimeSec'] = data.estTimeSec;
    params['estPartCost'] = data.estPartCost;
    params['estLabourCost'] = data.estLabourCost;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'AddROEstimation' + '?' + str,
      null,
      null,
      loader
    );
  }

  async GetRODDL(roID, loader = true) {
    let mid = await this.sqlite.getActiveUserId();

    let params = {};

    params['membershipId'] = mid;
    params['roID'] = roID;
    const str = this.serialize(params);
    return this.httpGetResponse('GetRO_DDL' + '?' + str, null, loader);
  }

  async GetVendorROPartsData(data, loader = false) {
    let mid = await this.sqlite.getActiveUserId();

    let params = Object.assign({}, data);

    params['membershipId'] = mid;
    params['roID'] = data.roID;
    params['page'] = data.page;
    params['size'] = 500;

    const str = this.serialize(params);
    return this.httpGetResponse(
      'GetROVendorPartCost' + '?' + str,
      null,
      loader
    );
  }

  async GetROAttachment(ROID, page = 0, size = 500, loader = true) {
    let mid = await this.sqlite.getActiveUserId();

    let params = {};
    params['membershipId'] = mid;
    params['inventory'] = ROID;
    params['Page'] = page;
    params['Size'] = size;

    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetROAttachmentPictures' + '?' + str,
      null,
      loader
    );
  }

  async postRoAttachments(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);

    return this.httpPostResponse(
      'InsertROAttachmentPictures' + '?' + str,
      data,
      null,
      loader
    );
  }

  async getROParts(roID, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['roId'] = roID;
    const str = this.serialize(params);
    return this.httpGetResponse('GetROPartList' + '?' + str, null, loader);
  }

  async GetCrAttachmentData(invID, page = 0, size = 500, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['InventoryId'] = invID;
    params['Page'] = page;
    params['Size'] = size;
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetCrAttachmentData' + '?' + str,
      null,
      loader
    );
  }

  async SearchPartDDL(storeId, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['storeid'] = storeId;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'SearchPartDDL' + '?' + str,
      null,
      null,
      loader
    );
  }

  async markRoAbandonment(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'MarkRoAbandonment' + '?' + str,
      data,
      null,
      loader
    );
  }
  async GetParts(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = Object.assign({}, data);
    params['membershipId'] = mid;
    params['StoreId'] = data['StoreId'];
    params['PartCatId'] = data['PartCatId'];
    params['PartSubCatId'] = data['PartSubCatId'];
    const str = this.serialize(params);
    return this.httpGetResponse('GetParts' + '?' + str, null, loader);
  }

  async SavePartRequest(data, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'SavePartRequest' + '?' + str,
      data,
      null,
      loader
    );
  }

  async CheckInventoryByStockOrVin(stockNo = '', vin, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['stockNo'] = stockNo;
    params['VIN'] = vin;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'CheckInventoryByStock_VIN' + '?' + str,
      null,
      null,
      loader
    );
  }

  async CompleteAudit(auditSummaryId, loader = true) {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;
    params['auditSummaryId'] = auditSummaryId;
    const str = this.serialize(params);
    return this.httpPostResponse(
      'CompleteAudit' + '?' + str,
      null,
      null,
      loader
    );
  }

  async CheckVin(
    Vin,
    AddNow,
    isScannedLoad = false,
    loader = false
  ): Promise<any> {
    let mid = await this.sqlite.getActiveUserId();
    let params = {};
    params['membershipId'] = mid;

    // let checkLoc = await this.geolocation.getCurrentLocationCoordinates();

    // if (checkLoc) {
    //   let long = checkLoc.lng;
    //   let lat = checkLoc.lat;
    //   if (long != null && lat != null) {
    //     params['longitude'] = long;
    //     params['latitude'] = lat;
    //   } else {
    //     let user: any = await this.sqlite.getActiveUser();
    //     if (user != null) {
    //       let storeId = user.storeId;
    //       params['storeId'] = storeId;
    //     }
    //   }
    // } else {
    //   let user: any = await this.sqlite.getActiveUser();
    //   if (user != null) {
    //     let storeId = user.storeId;
    //     params['storeId'] = storeId;
    //   }
    // }

    params['vinnumber'] = Vin;

    if (AddNow) {
      params['IsAddNow'] = true;
    } else {
      params['IsAddNow'] = false;
    }

    if (isScannedLoad) {
      params['isAudit'] = true;
    }
    const str = this.serialize(params);

    return this.httpGetResponse(
      'GetBasicTabDataFromVinWithDDL' + '?' + str,
      null,
      loader,
      false,
      'application/json',
      false
    );
  }
  serialize = (obj) => {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  };

  httpPostResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = false,
    contenttype = 'application/json',
    returnAsIs = false
  ) {
    return this.httpResponse(
      'post',
      key,
      data,
      id,
      showloader,
      showError,
      contenttype,
      returnAsIs
    );
  }

  httpGetResponse(
    key,
    id = null,
    showloader = false,
    showError = false,
    contenttype = 'application/json',
    returnAsIs = false
  ) {
    return this.httpResponse(
      'get',
      key,
      {},
      id,
      showloader,
      showError,
      contenttype,
      returnAsIs
    );
  }

  httpDeleteResponse(
    key,
    id = null,
    showloader = false,
    showError = false,
    contenttype = 'application/json',
    returnAsIs = false
  ) {
    return this.httpResponse(
      'delete',
      key,
      {},
      id,
      showloader,
      showError,
      contenttype,
      returnAsIs
    );
  }

  // default 'Content-Type': 'application/json',
  httpResponse(
    type = 'get',
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json',
    returnAsIs = false
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      this.ongoing = true;

      // tslint:disable-next-line:triple-equals
      if (showloader == true) {
        await this.utility.showLoader();
      }

      // tslint:disable-next-line:variable-name
      const _id = id ? '/' + id : '';
      const url = key + _id;
      // console.log('UURRLL', url);

      // console.log(contenttype);
      const reqOpts = {
        'Content-Type': contenttype,
      };

      // tslint:disable-next-line:triple-equals
      const seq =
        type == 'get'
          ? this.api.get(url, null, reqOpts)
          : type == 'delete'
          ? this.api.delete(url, null)
          : this.api.post(url, data, reqOpts);
      // console.log({ seq });

      (await seq).subscribe(
        (resObj: any) => {
          this.ongoing = false;
          // tslint:disable-next-line:triple-equals
          if (showloader == true) {
            this.utility.hideLoader();
          }
          // console.log({ resObj });
          console.log({ resObj });
          let failed = !resObj;
          let success = resObj && resObj.success;

          if (failed || !success) {
            resolve(null);
            this.utility.presentSuccessToast(
              resObj?.message ?? 'Something went wrong'
            );
          } else resolve(resObj);
        },
        (err) => {
          console.log('ERROR', err);

          this.ongoing = false;

          // console.log({ err });

          const error = err?.error;
          // tslint:disable-next-line:triple-equals
          if (showloader == true) {
            this.utility.hideLoader();
          }

          if (showError && err.status != 401) {
            if (err.error && err.error.data[0]) {
              this.utility.presentFailureToast(err.error.data[0]);
            } else {
              this.utility.presentFailureToast(error?.message);
            }
          }

          resolve(null);
        }
      );
    });
  }

  showFailure(err) {
    // console.error('ERROR', err);
    // tslint:disable-next-line:variable-name
    const _error = err ? err?.message : 'check logs';
    this.utility.presentFailureToast(_error);
  }
}
function loader(arg0: string, arg2: boolean, loader: any) {
  throw new Error('Function not implemented.');
}
