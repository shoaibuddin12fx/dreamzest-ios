import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    let token = localStorage.getItem('token')
    if (!token) {
      this.router.navigate(["pages/login"]);
      return false;
    }
    return true;
  }
}
