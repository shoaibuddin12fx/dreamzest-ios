import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';

@Injectable({
  providedIn: 'root',
})
export class StoresService {
  constructor(private network: NetworkService, private sqlite: SqliteService) {}

  // getStoresFromAPI
  getStoresFromAPI() {
    return new Promise(async (resolve) => {
      const res = await this.network.getStores();
      console.log(res);
      await this.setStoresInSqlite(res);
      resolve(res);
    });
  }

  // setStoreInSqlite
  setStoresInSqlite(stores) {
    return new Promise(async (resolve) => {
      await this.sqlite.insertStoreTable(stores);
      resolve(true);
    });
  }

  // getStoresFromSqlite

  async getStoresFromSqlite() {
    return new Promise(async (resolve) => {
      let res = (await this.sqlite.getAllStores()) as [];
      resolve(res);
    });
  }

  // getStoreById

  // setDefaultStore

  setDefaultStoreOfUser(id) {
    return new Promise(async (resolve) => {
      const res = await this.sqlite.setDefaultStoresOfUser(id);
      resolve(res[0]);
    });
  }

  getDefaultStoreOfUser() {
    return new Promise(async (resolve) => {
      const res = await this.sqlite.getDefaultStoresOfUser();
      resolve(res[0]);
    });
  }
}
