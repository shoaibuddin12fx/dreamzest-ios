import { Injectable } from '@angular/core';
import {
  IAPProduct,
  InAppPurchase2,
} from '@awesome-cordova-plugins/in-app-purchase-2/ngx';
import { BehaviorSubject } from 'rxjs';
import { UsersService } from './users.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root',
})
export class InAppPurchaseService {
  constructor(public store: InAppPurchase2, private utility: UtilityService) {}
  // products: IAPProduct[] = [];
  listener: BehaviorSubject<any>;
  productListener: BehaviorSubject<IAPProduct[]>;
  productIds = [
    {
      id: 'com.dreamzestApp.1.0.1.small',
      type: 'Small',
    },
  ];

  productPurchasedListener: BehaviorSubject<IAPProduct>;

  initialize() {
    // Only for debugging;
    this.store.verbosity = this.store.DEBUG;
    //
    this.listener = new BehaviorSubject('');
    this.productListener = new BehaviorSubject([]);
    this.productPurchasedListener = new BehaviorSubject(undefined);
    this.store.ready(() => {
      this.productListener.next(this.store.products);
    });
    this.registerProducts();
    this.setupListeners();
  }

  registerProducts() {
    this.productIds.forEach((val, index) => {
      this.store.register({
        id: val.id,
        type: this.store.NON_RENEWING_SUBSCRIPTION,
      });
    });
    this.store.refresh();
  }

  setupListeners() {
    // General query to all products //
    this.store
      .when('product')
      .approved(async (p: IAPProduct) => {
        if (p.transaction?.id) {
          // alert('Reached service');
          // alert('Approved: ' + p.title);
          this.productPurchasedListener.next(p);
          console.log('Product', p);
        }

        // Handle the product deliverable
        // if (p.id === PRODUCT_PRO_KEY) {
        //   this.isPro = true;
        // } else if (p.id === PRODUCT_GEMS_KEY) {
        //   this.gems += 100;
        // }
        // this.ref.detectChanges();

        return p.verify();
      })
      .cancelled(() => {
        this.presentAlert('Cancelled', 'User cancelled the subscription');
      })
      .verified((p: IAPProduct) => p.finish());
  }

  purchase(product: IAPProduct) {
    this.store.when(product).approved((p: IAPProduct) => {
      this.presentAlert('Success', `Purchased : ${p.title} Successfully!`);
    });
    this.store.order(product).then(
      (p) => {
        console.log('purchase', p);

        if (p.owned) console.log('Purchased', p);
        // Purchase in progress!
      },
      (e) => {
        this.presentAlert('Failed', `Failed to purchase: ${e}`);
      }
    );

    //Specific query for one ID
  }

  // To comply with AppStore rules
  restore() {
    this.store.refresh();
  }

  async presentAlert(header, message) {
    this.utility.showAlert(message, header);
  }

  removeListeners() {}
}
