import { TestBed } from '@angular/core/testing';

import { ApiParamsService } from './api-params.service';

describe('ApiParamsService', () => {
  let service: ApiParamsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiParamsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
