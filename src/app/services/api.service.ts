import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { Config } from '../config/main.config';
import { SqliteService } from './sqlite.service';
import { global } from '../config/constants';

// https://github.com/capacitor-community/http

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  BASE_URL: any = '';

  constructor(public http: HttpClient, public sqlite: SqliteService) {
    this.BASE_URL = global.BASE_URL;
  }

  async get(endpoint: string, params?: any, reqOpts?: any, fromCRM = false) {
    const options = {
      url: this.BASE_URL + endpoint,
      params: params,
    };

    console.log(options);

    return this.http.get(options.url, {});
  }

  async post(endpoint: string, body: any, reqOpts?: any, fromCRM = false) {
    const options = {
      url: this.BASE_URL + '/' + endpoint,
      method: 'post',
      data: JSON.stringify(body),
      headers: reqOpts,
    };
    console.log({ body });
    console.log({ options });
    return this.http.post(options.url, body);
  }

  put(endpoint: string, body: any, reqOpts?: any, fromCRM = false) {
    let serviceUrl = fromCRM ? Config.CRM_SERVICEURL : Config.SERVICEURL;
    return this.http.put(serviceUrl + '/' + endpoint, body, reqOpts);
  }

  async delete(endpoint: string, params?: any, reqOpts?: any, fromCRM = false) {
    let serviceUrl = fromCRM ? Config.CRM_SERVICEURL : Config.SERVICEURL;
    const options = {
      url: serviceUrl + '/' + endpoint,
      params: params,
    };

    console.log(options);

    return this.http.delete(options.url);
  }

  patch(endpoint: string, body: any, reqOpts?: any, fromCRM = false) {
    let serviceUrl = fromCRM ? Config.CRM_SERVICEURL : Config.SERVICEURL;
    return this.http.patch(serviceUrl + '/' + endpoint, body, reqOpts);
  }
}
