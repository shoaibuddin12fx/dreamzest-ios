import { Injectable, Injector } from '@angular/core';
import { BasePage } from '../pages/base-page/base-page';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
@Injectable({
  providedIn: 'root',
})
export class ImageService extends BasePage {

  
  constructor(injector: Injector) {
    super(injector);
  }


  getSnap(){
    return new Promise( async resolve => {
      const image = await Camera.getPhoto({
        quality: 90,
        width: 200,
        resultType: CameraResultType.DataUrl
      });
    
      resolve(image.dataUrl);
    
    })
  }
   
  // getImageFromGallerySelection(event): Promise<any> {
  //   return new Promise(async (resolve) => {
  //     let file = <File>event.target.files[0];
  //     if (file) {
  //       const mimeType = file.type;
  //       let reader = getFileReader();
  //       reader.readAsDataURL(file);
  //       reader.onload = async (e: any) => {
  //         this.image = e.target.result.split(',');
  //         console.log(this.image);
  //         resolve(this.image[1]);
  //       };
  //       console.log(file);
  //     }
  //   });
  // }

  capturePicture(): Promise<any> {
    return new Promise(async (resolve) => {
      const image = await Camera.getPhoto({
        quality: 90,
        allowEditing: true,
        resultType: CameraResultType.Uri,
      });

      let reader = getFileReader();
      let blob = new Blob([image.path]);
      resolve(blob);
    });
  }

  uploadPhoto(id, postData): Promise<any> {
    return new Promise(async (resolve) => {
      this.network.UploadCrComponentImageLinkToCr(id, postData).then((url) => {
        resolve(url);
      });
    });
  }
}

export function getFileReader(): FileReader {
  const fileReader = new FileReader();
  const zoneOriginalInstance = (fileReader as any)[
    '__zone_symbol__originalInstance'
  ];
  return zoneOriginalInstance || fileReader;
}
