import { Injectable, NgZone } from '@angular/core';
import * as firebase from 'firebase/app';
// import { auth } from "AngularFireAuth";
// import firebase from "firebase/app";
import 'firebase/auth';
import { Router } from '@angular/router';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import { AlertsService } from '../services/basic/alerts.service';
import { FirebaseAuthentication } from '@awesome-cordova-plugins/firebase-authentication/ngx';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Observable } from 'rxjs';
import { UtilityService } from './utility.service';
import { Capacitor } from '@capacitor/core';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  userData: any;
  constructor(
    public afStore: AngularFirestore,
    public ngFireAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone,
    public alerts: AlertsService,
    // public fbAuthentication : FirebaseAuthentication,
    public utility: UtilityService
  ) {
    // this.ngFireAuth.onAuthStateChanged((user) => {
    //   if (user) {
    //     this.userData = user;
    //     localStorage.setItem('user', JSON.stringify(this.userData));
    //     JSON.parse(localStorage.getItem('user'));
    //   } else {
    //     localStorage.setItem('user', null);
    //     JSON.parse(localStorage.getItem('user'));
    //   }
    // });
  }

  isUserLoggedIn() {
    return new Promise((resolve) => {
      this.ngFireAuth.onAuthStateChanged((user) => {
        if (user) {
          this.userData = user;
          localStorage.setItem('user', JSON.stringify(this.userData));
          resolve(this.userData);
        } else {
          localStorage.setItem('user', null);
          resolve(null);
        }
      });
    });
  }

  // Login in with email/password
  async SignInWeb(email, password) {
    return new Promise(async (res) => {
      console.log('running here');
      await this.ngFireAuth
        .signInWithEmailAndPassword(email, password)
        .then(async (value) => {
          let _user = await this.getUser();
          console.log('also running here');
          console.log(_user);
          res(_user);
        })
        .catch((reason) => {
          console.log('here is the reason', reason);
          this.utility.presentFailureToast(reason);
          res(null);
        });
    });
  }

  async SignIniOS(email, password) {
    console.log('SignIniOS');
    // return new Promise(res => {
    //   this.fbAuthentication.signInWithEmailAndPassword(email, password).then(async value => {
    //     let _user = await this.getCurrentUser();
    //     res(_user);
    //   }).catch(reason => {
    //     this.utility.presentFailureToast(reason);
    //     res(null);
    //   });
    // })
  }

  async changeEmail(data) {
    var uid = localStorage.getItem('user');
    console.log('user', uid);
    // const userRef: AngularFirestoreDocument<any> = this.afStore.doc(`users/${user.uid}`);
    // console.log("sasas", userData)
    // return userRef.set(userData, {
    //   merge: true
    // })
  }

  // Register user with email/password
  RegisterUser(email, password) {
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password);
  }
  // Email verification when new user register
  SendVerificationMail() {
    // return this.ngFireAuth.currentUser.sendEmailVerification()
    // .then(() => {
    //   this.router.navigate(['verify-email']);
    // })
  }
  // Recover password
  PasswordRecover(passwordResetEmail) {
    return this.ngFireAuth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert(
          'Password reset email has been sent, please check your inbox.'
        );
      })
      .catch((error) => {
        window.alert(error);
      });
  }
  // Returns true when user is looged in
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null && user.emailVerified !== false ? true : false;
  }
  // Returns true when user's email is verified
  get isEmailVerified(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user.emailVerified !== false ? true : false;
  }
  // Sign in with Gmail
  // GoogleAuth() {
  //   return this.AuthLogin(new GoogleAuthProvider());
  // }
  // // Auth providers
  // AuthLogin(provider) {
  //   return this.ngFireAuth.signInWithPopup(provider)
  //     .then((result) => {
  //       this.ngZone.run(() => {
  //         this.router.navigate(['dashboard']);
  //       })
  //       this.SetUserData(result.user);
  //     }).catch((error) => {
  //       window.alert(error)
  //     })
  // }
  // Store user in localStorage
  SetUserData(user, userData) {
    const userRef: AngularFirestoreDocument<any> = this.afStore.doc(
      `users/${user.uid}`
    );
    console.log('sasas', userData);
    return userRef.set(userData, {
      merge: true,
    });
  }

  getUserData() {
    return new Promise(async (res, rej) => {
      let user = await this.getUser();
      console.log('USER_FB', user);

      this.afStore
        .doc(`users/${user['uid']}`)
        .valueChanges()
        .subscribe(
          (snapshot) => {
            console.log('USER_CHANGES', snapshot);
            res(snapshot);
          },
          (error) => {
            alert(error.toString());
            res(null);
          }
        );
    });
  }

  // getCurrentUser() {
  //   return this.fbAuthentication.getCurrentUser();
  // }

  getUser() {
    return new Promise((res, rej) => {
      this.ngFireAuth.user.subscribe(
        (_user) => {
          res(_user);
        },
        (error) => {
          alert(error);
          rej(error);
        }
      );
    });

    //  return this.ngFireAuth.getCurrentUser();
  }

  getAuthUser() {
    if (Capacitor.getPlatform() === 'ios')
      // return this.getCurrentUser();
      return this.getUser();
  }

  // firebase.auth()
  // .signInWithEmailAndPassword('you@domain.com', 'correcthorsebatterystaple')
  // .then(function(userCredential) {
  //     userCredential.user.updateEmail('newyou@domain.com')
  // })

  // Sign-out
  SignOut() {
    return new Promise((resolve) => {
      this.ngFireAuth.signOut().then(() => {
        localStorage.removeItem('user');
        this.router.navigate(['start']);
        resolve(true);
      });
    });
    // return this.ngFireAuth.signOut().then(() => {
    //   localStorage.removeItem('user');
    //   this.router.navigate(['start']);
    // });
  }
}
