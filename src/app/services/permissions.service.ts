import { Injectable } from '@angular/core';
import { SqliteService } from './sqlite.service';


@Injectable({
    providedIn: 'root'
})
export class PermissionsService {

    permissions: any = [];

    constructor(
        private sqlite: SqliteService,
    ) {
    }

    async checkPermission(permission) {
        if (this.permissions.length == 0) {
            await this.getPermissions();
        }
        return this.permissions.some((x: any) => (x).PermissionName === permission)
    }

    async getPermissions() {
        if (!this.permissions.length) {
            this.permissions = await this.sqlite.getAllPermissions();
        }
    }

}